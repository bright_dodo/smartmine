import {Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../app.store';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Person} from '../_models/person';
import {Assignment} from '../_models/assignment';
import {IPersonType} from '../_models/person-type';
import {FETCH_PERSONTYPES_SUCCESS, UPDATE_SELECTED_PERSON_TYPE_ATTRIBUTES} from '../lampsman/lampsman.action';
import {ToastrService} from 'ngx-toastr';
import {AttributeTransformPipe} from '../_pipes/attribute-transform.pipe';
import {ResultList} from '../_models/result-list';

@Injectable()
export class PersonService {
  private personBaseUrl = environment.api.base + 'person/';

  constructor(injector: Injector,
              private http: HttpClient,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService) {
    // super(Person, 'person', injector);
  }

  createPerson(person: any): Observable<Person> {
    return this.http.post<Person>(this.personBaseUrl, person);
  }

  editPerson(person: any): Observable<Person> {
    return this.http.put<Person>(this.personBaseUrl + 'edit', person);
  }

  getPersonByID(personId: string): Observable<Person> {
    return this.http.get<Person>(this.personBaseUrl + personId);
  }

  searchPerson(data: any): Observable<ResultList<Person>> {
     return this.http.post<any>(this.personBaseUrl + 'search', data);
/*    const map = new AttributeTransformPipe().transform(data);
    const options = {params: map};
    return this.search('', options);*/
  }

  getAssignmentsForPerson(personId: string): Observable<ResultList<Assignment>> {
    return this.http.get<any>(this.personBaseUrl + personId + '/assignments');
  }

  getAssignmentHistoryForPerson(personId: string): Observable<ResultList<Assignment>> {
    return this.http.get<any>(this.personBaseUrl + personId + '/assignments/history');
  }

  fetchPersonTypes() {
    return this.http.get<IPersonType[]>(this.personBaseUrl + 'types')
      .subscribe((response: any[]) => {
          this.ngRedux.dispatch({type: FETCH_PERSONTYPES_SUCCESS, personTypes: response});
        },
        error => {
          this.toastr.error(error.toString(), 'Error');
        });
  }

  fetchAllPersons(): Observable<Person[]> {
    return this.http.get<Person[]>(this.personBaseUrl);
  }

  getPersonTypeByName(name: string): Observable<IPersonType> {
    return this.http.get<IPersonType>(this.personBaseUrl + 'types/' + name);
  }

  getPersonTypeAttributes(personType: string) {
    return this.http.get(this.personBaseUrl + 'types/' + personType + '/attributes')
      .subscribe((data: any[]) => {
          this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON_TYPE_ATTRIBUTES, attributes: data});
        },
        error => this.toastr.error(error.toString(), 'Error!'),
        () => console.log('done loading person type Attributes'));
  }
}
