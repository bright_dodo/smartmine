import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/index';
import {ISelectItem} from '../_models/select-item';
import {ResultList} from '../_models/result-list';

@Injectable({
  providedIn: 'root'
})
export class MovementService {
  private movementBaseUrl = environment.api.base + 'movement/';

  constructor(private http: HttpClient) {
  }

  getMovementTypes(): Observable<ISelectItem[]> {
    return this.http.get<any>(this.movementBaseUrl + 'types');
  }

  searchMovements(data: any): Observable<ResultList<any>> {
    return this.http.post<any>(this.movementBaseUrl + 'search', data);
  }
}
