import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/index';
import {ISelectItem} from '../_models/select-item';
import {ResultList} from '../_models/result-list';
import {TrackingEvent} from '../_models/tracking-event';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {
  private trackingBaseUrl = environment.api.base + 'tracking/';

  constructor(private http: HttpClient) { }

  getTrackingTypes(): Observable<ISelectItem[]> {
    return this.http.get<any>(this.trackingBaseUrl + 'types');
  }

  search(data: any): Observable<ResultList<TrackingEvent>> {
    return this.http.post<any>(this.trackingBaseUrl + 'search', data);
  }
}
