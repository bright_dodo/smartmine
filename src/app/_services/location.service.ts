import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Location} from '../_models/location';

@Injectable()
export class LocationService {

  private locationBaseUrl = environment.api.base + 'location/';
  constructor(private http: HttpClient) { }

  listAllLocations(): Observable<Location[]> {
    return this.http.get<Location[]>(this.locationBaseUrl);
  }

  getLocationsByType(typeName: string): Observable<Location[]> {
    return this.http.get<Location[]>(this.locationBaseUrl + 'types/' + typeName);
  }

  getListOfTagsAtLocation(locationName: string): Observable<any> {
    return this.http.get(this.locationBaseUrl + locationName);
  }
}
