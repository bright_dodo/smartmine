import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import * as FileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class FileDownloadService {

  constructor(private http: HttpClient) { }

  downloadFile(url: string, filename: string) {
    this.http.get(url, {responseType: 'blob'})
      .subscribe((file: Blob) => {
        FileSaver.saveAs(file, filename);
      });
  }
}
