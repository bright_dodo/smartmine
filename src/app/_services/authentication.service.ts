import {Injectable} from '@angular/core';
import {IToken} from '../_models/token';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {NgRedux} from '@angular-redux/store';
import {Observable, BehaviorSubject} from 'rxjs';
import {IAppState} from '../app.store';
import {LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT} from '../users/users.actions';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  token: IToken;
  cachedRequests: Array<HttpRequest<any>> = [];
  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(private http: HttpClient,
              private ngRedux: NgRedux<IAppState>,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.loggedIn = store.user.isLoggedIn;
      this.token = store.user.token;
    });

    if (this.authenticated()) {
      this.setLoggedIn(true);
    } else {
      // this.logout();
    }
  }

  obtainAccessToken(loginData): Observable<any> {
    const params = new URLSearchParams();
    params.append('grant_type', 'password');
    params.append('username', loginData.username);
    params.append('password', loginData.password);
    params.append('registrationtoken', 'Android 1');

    return this.http.post(environment.api.security + 'oauth/token', params.toString(), this.getHeaders());
  }

  refreshAccessToken(): Observable<any> {
    const params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', localStorage.getItem('refresh_token'));
    params.append('registrationtoken', 'Android 1');

    return this.http.post<IToken>(environment.api.security + 'oauth/token', params.toString(), this.getHeaders());
  }

  isLoggedIn(): boolean {
    return this.loggedIn;
  }

  setLoggedIn(value: boolean) {
    // Update login status subject
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  retrieveToken(): any {
    if ((localStorage.getItem('access_token') !== '' || localStorage.getItem('access_token')) && this.authenticated()) {
      return localStorage.getItem('access_token');
    }else {
      // this.logout();
    }
    return null;
  }

  public collectFailedRequest(request): void {
    this.cachedRequests.push(request);
  }

  public retryFailedRequests(): void {
    // retry the requests. this method can
    // be called after the token is refreshed
  }

  authenticated(): boolean {
    // Check if current date is greater than expiration
    const expiresAt = JSON.parse(localStorage.getItem('expires_in'));
    return Date.now() < expiresAt;
  }

  logout() {
    console.log('ndiani ati tibude?');
    this.ngRedux.dispatch({type: LOGOUT});
    localStorage.removeItem('access_token');
    localStorage.removeItem('token_type');
    localStorage.removeItem('refresh_token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('expires_in');
    localStorage.removeItem('scope');
    localStorage.removeItem('registration_token');
    localStorage.removeItem('jti');
    this.router.navigate(['/login']);
  }

  setSession(authToken: IToken) {
    // Save session data and update login status subject
    const expTime = authToken.expires_in * 1000 + Date.now();
    localStorage.setItem('access_token', authToken.access_token);
    localStorage.setItem('token_type', authToken.token_type);
    localStorage.setItem('refresh_token', authToken.refresh_token);
    localStorage.setItem('user_id', authToken.user_id);
    localStorage.setItem('expires_in', JSON.stringify(expTime));
    localStorage.setItem('scope', authToken.scope);
    localStorage.setItem('registration_token', authToken.registration_token);
    localStorage.setItem('jti', authToken.jti);
    this.setLoggedIn(true);
  }

  private getHeaders(): any {
    // 'Authorization': 'Basic ' + btoa('ungaClientIdPassword:secret')
    const headers = new HttpHeaders({
      'Authorization': 'Basic dW5nYTpzZWNyZXQ=',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true'
    });
    headers.append('Accept', 'application/json');
    /*    const options: RequestOptions = new RequestOptions();
        options.headers = headers;*/
    return {headers: headers};
  }

  private getTokenFromSession(): IToken {
    const itoken: IToken = {
      'access_token': localStorage.getItem('access_token'),
      'token_type': localStorage.getItem('token_type'),
      'refresh_token': localStorage.getItem('refresh_token'),
      'user_id': localStorage.getItem('user_id'),
      'expires_in': JSON.parse(localStorage.getItem('expires_in')),
      'scope': localStorage.getItem('scope'),
      'registration_token': localStorage.getItem('registration_token'),
      'jti': localStorage.getItem('jti'),
    };
    return itoken;
  }

}
