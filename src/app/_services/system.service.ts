import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {SystemDetails} from '../_models/system-details';

@Injectable()
export class SystemService {

  constructor(private http: HttpClient) { }

  getSystemDetails(): Observable<SystemDetails> {
    return this.http.get<SystemDetails>(environment.api.base + 'system-info');
  }

}
