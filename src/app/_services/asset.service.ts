import {Injectable, Injector} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Asset} from '../_models/asset';
import {Assignment} from '../_models/assignment';
import {IAssetType} from '../_models/asset-type';
import {Person} from '../_models/person';
import {FETCH_ASSETTYPES_SUCCESS, UPDATE_CLASS_ASSET_TYPES, UPDATE_SELECTED_ASSET_TYPE_ATTRIBUTES} from '../lampsman/lampsman.action';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../app.store';
import {ToastrService} from 'ngx-toastr';
import {IAttribute} from '../_models/attribute';
import {ResultList} from '../_models/result-list';

@Injectable()
export class AssetService {
  private assetBaseUrl = environment.api.base + 'equipment/';

  constructor(injector: Injector,
              private http: HttpClient,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService) {
    // super(Asset, 'equipment/', injector);
  }

  fetchAllAssets(): Observable<Asset[]> {
    return this.http.get<Asset[]>(this.assetBaseUrl);
  }

  createAsset(asset: any): Observable<Asset> {
    return this.http.post<Asset>(this.assetBaseUrl + 'create', asset);
  }

  editAsset(asset: any): Observable<Asset> {
    return this.http.put<Asset>(this.assetBaseUrl, asset);
  }

  searchAsset(data: any): Observable<ResultList<Asset>> {
    return this.http.post<any>(this.assetBaseUrl + 'search', data);
  }

  getAssetByID(assetId: string): Observable<Asset> {
    return this.http.get<Asset>(this.assetBaseUrl + assetId);
  }

  getAssetByTypeAndAllocationStatus(assetType: string, isSpare: string, isAssigned: string): Observable<ResultList<Asset>> {
     return this.http.get<any>(this.assetBaseUrl + assetType + '/' + isSpare + '/' + isAssigned);
    // return this.customQuery(assetType + '/' + isSpare + '/' + isAssigned);
  }

  assignAsset(assignment: any): Observable<ResultList<Assignment>> {
    return this.http.post<any>(this.assetBaseUrl + 'assign', assignment);
  }

  deassignAsset(assignment: any): Observable<ResultList<Assignment>> {
    return this.http.put<any>(this.assetBaseUrl + 'deassign', assignment);
  }

  getAssetTypeByName(name: string): Observable<IAssetType> {
    return this.http.get<IAssetType>(this.assetBaseUrl + 'types/' + name);
  }

  fetchAssetTypes() {
    return this.http.get<ResultList<IAssetType>>(this.assetBaseUrl + 'types')
      .subscribe(response => {
          const types = response._embedded['assetTypes'];
          this.ngRedux.dispatch({type: FETCH_ASSETTYPES_SUCCESS, assetTypes: types});
        },
        error2 => {
          console.log(error2);
          this.toastr.error('There was an error fetching asset types from server', 'Fetch Error!');
        });
  }

  getAssetTypesByClass(className: string): Observable<IAssetType[]> {
    return this.http.get<IAssetType[]>(this.assetBaseUrl + className + '/types');
  }

  getAssetTypeAttributes(assetType: string): Observable<IAttribute[]> {
    return this.http.get<IAttribute[]>(this.assetBaseUrl + 'types/' + assetType + '/attributes');
    /*      .subscribe((attributes: any[]) => {
            console.log(attributes);
              this.ngRedux.dispatch({type: UPDATE_SELECTED_ASSET_TYPE_ATTRIBUTES, attributes: attributes});
            },
            error => this.toastr.error(error.toString(), 'Error'),
            () => console.log('done loading equipment type Attributes'));*/
  }

  getAssetTypeCount(): Observable<any> {
    return this.http.get(this.assetBaseUrl + 'count');
  }

  getAssetTypeSummaries(): Observable<any> {
    return this.http.get(this.assetBaseUrl + 'summary');
  }

  getPersonForAsset(assetId: string): Observable<Person> {
    return this.http.get<Person>(this.assetBaseUrl + assetId + '/person');
  }

  getAssignmentsForAsset(assetId: string): Observable<Assignment[]> {
    return this.http.get<Assignment[]>(this.assetBaseUrl + assetId + '/assignments');
  }

  findAssignmentForAsset(assetId: string): Observable<Assignment> {
    return this.http.get<Assignment>(this.assetBaseUrl + assetId + '/assignment');
  }

  getAssignmentHistoryForAsset(assetId: string): Observable<ResultList<Assignment>> {
    return this.http.get<any>(this.assetBaseUrl + assetId + '/assignments/history');
  }
}
