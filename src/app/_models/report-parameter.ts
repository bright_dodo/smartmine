export interface ReportParameter {
  id: number;
  name: string;
  title: string;
  defaultValue: string;
  disabled: boolean;
  expression: string;
  feature: string;
  formatting: string[];
  inQuery: boolean;
  options: string;
  required: boolean;
  type: any;
}
