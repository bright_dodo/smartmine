export interface IAttribute {
  id: number;
  index: number;
  name: string;
  description: string;
  required: boolean;
  keyValue: boolean;
  disableHistory: boolean;
  type: {
    description: string;
    name: string;
    systemType: string;
  };
}

