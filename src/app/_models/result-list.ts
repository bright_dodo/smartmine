export interface ResultList<T> {
  _embedded: T[];
  _links: any;
  page: any;
}
