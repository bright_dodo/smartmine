export interface IToken {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  user_id: string;
  registration_token: string;
  jti: string;
}
