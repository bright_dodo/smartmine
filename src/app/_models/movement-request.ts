import {Person} from './person';

export class MovementRequest {
  agent: any;
  location: any;
  person: Person;
  requestTime: any;
  uuid: string;
  type: any;
}
