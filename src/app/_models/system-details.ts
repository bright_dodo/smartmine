export class SystemDetails {
  applicationName: string;
  organisation: string;
  version: string;
  lastUpdated: any;
}
