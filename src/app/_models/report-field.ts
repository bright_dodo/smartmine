export interface ReportField {
  expression: string;
  id: number;
  name: string;
  options: any;
  pattern: string;
  showEmptyColumn: boolean;
  sortable: boolean;
  title: string;
  type: any;
  width: number;
}
