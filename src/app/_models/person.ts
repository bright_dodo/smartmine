import {IPersonType} from './person-type';
import {IAttributeMap} from './attribute-map';
import {IAttr} from './IAttr';

export class Person {
  personId: string;
  name: string;
  firstname: string;
  occupation: string;
  personType: IPersonType;
  shaft: string;
  shift: string;
  id: number;
  type: any;
  attr: IAttr[];
  attributeMap: IAttributeMap;

  toString(): string {
    return this.name + ' ' + this.personId;
  }
}
