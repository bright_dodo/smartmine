import {ReportParameter} from './report-parameter';
import {ReportField} from './report-field';

export class ReportConfig {
  derivedParameters: ReportParameter[];
  parameters: ReportParameter[];
  fields: ReportField[];
  query: string;
  title: string;
  description: string;
  name: string;
  feature: string;
  evaluatedOptions: any;
  fieldsSetup: string[];
  hidden: boolean;
  options: any;
  outputConfig: any;
  providerClass: string;
  reportType: string;
  resultClass: string;
  skipSort: boolean;
  subReports: boolean;
  maxResults: number;
}
