import {IAttribute} from './attribute';

export interface ILocationType {
  name: string;
  description: string;
  attributes: IAttribute[];
  properties: any[];
}
