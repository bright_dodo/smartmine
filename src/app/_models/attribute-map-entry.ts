export interface IAttributeMapEntry {
  key: string;
  value: string;
}
