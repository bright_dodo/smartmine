import {Asset} from './asset';
import {MovementEvent} from './movement-event';
import {MovementRequest} from './movement-request';

export class MovementResponse {
  assets: Asset[];
  assetRef: string;
  events: any[];
  movement: MovementEvent;
  request: MovementRequest;
  responseCode: string;
  responseMessage: string;
  responseTime: any;
  uuid: string;
  type: any;
  status: string;
}
