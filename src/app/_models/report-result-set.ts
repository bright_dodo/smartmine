import {ReportDataTableItem} from '../reports/report-data-table/report-data-table-datasource';

export interface ReportResultSet {
  criteria: any;
  displayFields: any;
  fields: any;
  name: string;
  rows: ReportDataTableItem[];
  title: string;
}
