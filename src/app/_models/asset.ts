import {IAssetType} from './asset-type';
import {IAttributeMap} from './attribute-map';
import {IAttr} from './IAttr';

export class Asset {
  id: number;
  assetId: string;
  allocStatus: string;
  type: any;
  assetType: IAssetType;
  assignmentType: string;
  status: string;
  attr: IAttr[];
  attributeMap: IAttributeMap;
  createdDate: any;
  lastUpdate: any;
  removedDate: any;
}
