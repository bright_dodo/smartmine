import {IAttribute} from './attribute';

export interface IPersonType {
  name: string;
  description: string;
  attributes: IAttribute[];
  properties: any[];
  statusLookup: string;
}
