import {IAttributeMapEntry} from './attribute-map-entry';

export interface IAttributeMap {
  entry: IAttributeMapEntry[];
}
