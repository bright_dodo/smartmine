export interface IAttr {
  id: number;
  name: string;
  value: any;
}
