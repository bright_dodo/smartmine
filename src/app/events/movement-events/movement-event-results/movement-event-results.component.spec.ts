
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementEventResultsComponent } from './movement-event-results.component';

describe('MovementEventResultsComponent', () => {
  let component: MovementEventResultsComponent;
  let fixture: ComponentFixture<MovementEventResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementEventResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovementEventResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
