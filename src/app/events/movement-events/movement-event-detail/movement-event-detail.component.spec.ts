import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementEventDetailComponent } from './movement-event-detail.component';

describe('MovementEventDetailComponent', () => {
  let component: MovementEventDetailComponent;
  let fixture: ComponentFixture<MovementEventDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementEventDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementEventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
