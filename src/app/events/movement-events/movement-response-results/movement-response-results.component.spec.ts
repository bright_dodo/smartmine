
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementResponseResultsComponent } from './movement-response-results.component';

describe('MovementResponseResultsComponent', () => {
  let component: MovementResponseResultsComponent;
  let fixture: ComponentFixture<MovementResponseResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementResponseResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovementResponseResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
