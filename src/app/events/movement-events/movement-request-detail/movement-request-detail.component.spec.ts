import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementRequestDetailComponent } from './movement-request-detail.component';

describe('MovementRequestDetailComponent', () => {
  let component: MovementRequestDetailComponent;
  let fixture: ComponentFixture<MovementRequestDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
