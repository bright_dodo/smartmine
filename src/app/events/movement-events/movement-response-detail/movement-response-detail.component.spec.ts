import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementResponseDetailComponent } from './movement-response-detail.component';

describe('MovementResponseDetailComponent', () => {
  let component: MovementResponseDetailComponent;
  let fixture: ComponentFixture<MovementResponseDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementResponseDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementResponseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
