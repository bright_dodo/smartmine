import {
  UPDATE_MEASUREMENTS,
  UPDATE_MVT_EVENTS,
  UPDATE_MVT_REQUESTS, UPDATE_MVT_RESPONSES,
  UPDATE_SHOW_MVT_EVENTS,
  UPDATE_SHOW_MVT_REQUESTS,
  UPDATE_SHOW_MVT_RESPONSES, UPDATE_TRACKING_EVENTS
} from './events.actions';
import {tassign} from 'tassign';
import {MovementEvent} from '../_models/movement-event';
import {MovementRequest} from '../_models/movement-request';
import {MovementResponse} from '../_models/movement-response';
import {TrackingEvent} from '../_models/tracking-event';
import {Measurement} from '../_models/measurement';

export interface IEventsState {
  showMovementEvents: boolean;
  showMovementRequests: boolean;
  showMovementResponses: boolean;
  movementEvents: MovementEvent[];
  movementRequests: MovementRequest[];
  movementResponses: MovementResponse[];
  trackingEvents: TrackingEvent[];
  measurements: Measurement[];
}

export const EVENTS_INITIAL_STATE: IEventsState = {
  showMovementEvents: false,
  showMovementRequests: false,
  showMovementResponses: false,
  movementEvents: [],
  movementRequests: [],
  movementResponses: [],
  trackingEvents: [],
  measurements: []
};

function updateShowMovementEvents(state: IEventsState, action: any) {
  return tassign(state, {showMovementEvents: action.show, showMovementRequests: false, showMovementResponses: false});
}

function updateShowMovementRequests(state: IEventsState, action: any) {
  return tassign(state, {showMovementRequests: action.show, showMovementEvents: false, showMovementResponses: false});
}

function updateShowMovementResponses(state: IEventsState, action: any) {
  return tassign(state, {showMovementResponses: action.show, showMovementEvents: false, showMovementRequests: false});
}

function updateMovementEvents(state: IEventsState, action: any) {
  return tassign(state, {movementEvents: action.events});
}

function updateMovementRequests(state: IEventsState, action: any) {
  return tassign(state, {movementRequests: action.requests});
}

function updateMovementResponses(state: IEventsState, action: any) {
  return tassign(state, {movementResponses: action.responses});
}

function updateTrackingEvents(state: IEventsState, action: any) {
  return tassign(state, {trackingEvents: action.events});
}

function updateMeasurements(state: IEventsState, action: any) {
  return tassign(state, {measurements: action.measurements});
}

export function eventReducer(state: IEventsState = EVENTS_INITIAL_STATE, action: any): IEventsState {
  switch (action.type) {
    case UPDATE_SHOW_MVT_EVENTS:
      return updateShowMovementEvents(state, action);
    case UPDATE_SHOW_MVT_REQUESTS:
      return updateShowMovementRequests(state, action);
    case UPDATE_SHOW_MVT_RESPONSES:
      return updateShowMovementResponses(state, action);
    case UPDATE_MVT_EVENTS:
      return updateMovementEvents(state, action);
    case UPDATE_MVT_REQUESTS:
      return updateMovementRequests(state, action);
    case UPDATE_MVT_RESPONSES:
      return updateMovementResponses(state, action);
    case UPDATE_TRACKING_EVENTS:
      return updateTrackingEvents(state, action);
    case UPDATE_MEASUREMENTS:
      return updateMeasurements(state, action);
    default:
      return state;
  }
}
