import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EventsRoutingModule} from './events-routing.module';
import {EventsComponent} from './events/events.component';
import {TrackingEventsComponent} from './tracking-events/tracking-events.component';
import {MeasurementEventsComponent} from './measurement-events/measurement-events.component';
import {MovementEventsComponent} from './movement-events/movement-events.component';
import {SharedModule} from '../shared/shared.module';
import {MovementEventResultsComponent} from './movement-events/movement-event-results/movement-event-results.component';
import {MatTableModule, MatPaginatorModule, MatSortModule} from '@angular/material';
import {TrackingEventResultsComponent} from './tracking-events/tracking-event-results/tracking-event-results.component';
import {MeasurementResultsComponent} from './measurement-events/measurement-results/measurement-results.component';
import {MovementRequestResultsComponent} from './movement-events/movement-request-results/movement-request-results.component';
import {MovementResponseResultsComponent} from './movement-events/movement-response-results/movement-response-results.component';
import {MovementEventDetailComponent} from './movement-events/movement-event-detail/movement-event-detail.component';
import { MeasurementEventDetailComponent } from './measurement-events/measurement-event-detail/measurement-event-detail.component';
import { TrackingEventDetailComponent } from './tracking-events/tracking-event-detail/tracking-event-detail.component';
import { MovementRequestDetailComponent } from './movement-events/movement-request-detail/movement-request-detail.component';
import { MovementResponseDetailComponent } from './movement-events/movement-response-detail/movement-response-detail.component';

@NgModule({
  imports: [
    CommonModule,
    EventsRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  declarations: [
    EventsComponent,
    TrackingEventsComponent,
    MeasurementEventsComponent,
    MovementEventsComponent,
    MovementEventResultsComponent,
    TrackingEventResultsComponent,
    MeasurementResultsComponent,
    MovementRequestResultsComponent,
    MovementResponseResultsComponent,
    MovementEventDetailComponent,
    MeasurementEventDetailComponent,
    TrackingEventDetailComponent,
    MovementRequestDetailComponent,
    MovementResponseDetailComponent
  ],
  entryComponents: [
    MovementEventDetailComponent,
    MovementRequestDetailComponent,
    MovementResponseDetailComponent,
    MeasurementEventDetailComponent,
    TrackingEventDetailComponent
  ]
})
export class EventsModule {
}
