import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Measurement} from '../../../_models/measurement';

@Component({
  selector: 'sm-measurement-event-detail',
  templateUrl: './measurement-event-detail.component.html',
  styleUrls: ['./measurement-event-detail.component.css']
})
export class MeasurementEventDetailComponent implements OnInit {
  @Input() measurement: Measurement;
  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

}
