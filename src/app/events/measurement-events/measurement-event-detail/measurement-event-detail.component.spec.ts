import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementEventDetailComponent } from './measurement-event-detail.component';

describe('MeasurementEventDetailComponent', () => {
  let component: MeasurementEventDetailComponent;
  let fixture: ComponentFixture<MeasurementEventDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementEventDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementEventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
