import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MeasurementResultsDataSource } from './measurement-results-datasource';
import {MovementEventDetailComponent} from '../../movement-events/movement-event-detail/movement-event-detail.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MeasurementEventDetailComponent} from '../measurement-event-detail/measurement-event-detail.component';

@Component({
  selector: 'sm-measurement-results',
  templateUrl: './measurement-results.component.html',
  styleUrls: ['./measurement-results.component.css']
})
export class MeasurementResultsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MeasurementResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['time', 'type', 'assetId', 'location', 'value', 'options'];

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.dataSource = new MeasurementResultsDataSource(this.paginator, this.sort);
  }

  viewDetails(measurement: any) {
    const measurementDetailModalRef = this.modalService.open(MeasurementEventDetailComponent);
    measurementDetailModalRef.componentInstance.measurement = measurement;
  }

}
