
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingEventResultsComponent } from './tracking-event-results.component';

describe('TrackingEventResultsComponent', () => {
  let component: TrackingEventResultsComponent;
  let fixture: ComponentFixture<TrackingEventResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingEventResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrackingEventResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
