import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocationService} from '../../_services/location.service';
import {TrackingService} from '../../_services/tracking.service';
import {AssetSearchModalComponent} from '../../lampsman/asset/asset-search-modal/asset-search-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonSearchModalComponent} from '../../lampsman/person/person-search-modal/person-search-modal.component';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {UPDATE_TRACKING_EVENTS} from '../events.actions';

@Component({
  selector: 'sm-tracking-events',
  templateUrl: './tracking-events.component.html',
  styleUrls: ['./tracking-events.component.css']
})
export class TrackingEventsComponent implements OnInit {
  searchTrackingForm: FormGroup;
  trackingTypes: any[];
  locations: any[];

  a2eOptions = {
    format: 'DD.MM.YYYY HH:mm',
    maxDate: new Date()
  };

  constructor(private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private locationService: LocationService,
              private trackingService: TrackingService,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.initSearchTrackingForm();
    this.getLocations();
    this.getTrackingTypes();
  }

  private getTrackingTypes() {
    this.trackingService.getTrackingTypes()
      .subscribe(response => {
        this.trackingTypes = response;
      });
  }

  private initSearchTrackingForm() {
    this.searchTrackingForm = this.formBuilder.group({
      trackingType: [''],
      location: [''],
      startTime: [''],
      endTime: [''],
      personId: [''],
      assetId: [''],
      maxResults: ['200']
    });
  }

  private getLocations() {
    this.locationService.listAllLocations()
      .subscribe(response => {
        this.locations = response;
      });
  }

  searchTracking(form: any) {
    this.trackingService.search(form)
      .subscribe(response => {
        if (response && response._embedded['trackingEventResources']) {
          this.ngRedux.dispatch({type: UPDATE_TRACKING_EVENTS, events: response._embedded['trackingEventResources']});
        }
      });
  }

  openAssetSearchModal() {
    const assetSearchModalRef = this.modalService.open(AssetSearchModalComponent, {size: 'lg'});

    assetSearchModalRef.result.then(result => {
      if (result.success) {
        this.searchTrackingForm.controls['assetId'].setValue(result.assetId);
      }
    });
  }

  openPersonSearchModal() {
    const personSearchModalRef = this.modalService.open(PersonSearchModalComponent, {size: 'lg'});

    personSearchModalRef.result.then(result => {
      if (result.success) {
        this.searchTrackingForm.controls['personId'].setValue(result.personId);
      }
    });
  }
}
