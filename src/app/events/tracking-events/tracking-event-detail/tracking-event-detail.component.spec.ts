import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingEventDetailComponent } from './tracking-event-detail.component';

describe('TrackingEventDetailComponent', () => {
  let component: TrackingEventDetailComponent;
  let fixture: ComponentFixture<TrackingEventDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingEventDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingEventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
