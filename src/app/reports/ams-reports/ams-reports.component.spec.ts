import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmsReportsComponent } from './ams-reports.component';

describe('AmsReportsComponent', () => {
  let component: AmsReportsComponent;
  let fixture: ComponentFixture<AmsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
