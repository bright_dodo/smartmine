import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdiReportsComponent } from './gdi-reports.component';

describe('GdiReportsComponent', () => {
  let component: GdiReportsComponent;
  let fixture: ComponentFixture<GdiReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdiReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdiReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
