
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDataTableComponent } from './report-data-table.component';

describe('ReportDataTableComponent', () => {
  let component: ReportDataTableComponent;
  let fixture: ComponentFixture<ReportDataTableComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDataTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReportDataTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
