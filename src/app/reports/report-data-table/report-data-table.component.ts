import {Component, Input, OnChanges, OnInit, SimpleChange, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {ReportDataTableDataSource} from './report-data-table-datasource';
import {ReportConfig} from '../../_models/report-config';
import {ReportResultSet} from '../../_models/report-result-set';

@Component({
  selector: 'sm-report-data-table',
  templateUrl: './report-data-table.component.html',
  styleUrls: ['./report-data-table.component.css']
})
export class ReportDataTableComponent implements OnInit, OnChanges {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @Input() resultSet: ReportResultSet;
  dataSource: ReportDataTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
    // displayedColumns = ['id', 'name'];
  displayedColumns = [];

  ngOnInit() {
    if (this.resultSet) {
      this.prepareTableColumns();
    }
    this.dataSource = new ReportDataTableDataSource(this.paginator, this.sort, this.resultSet.rows);
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }): void {
    if (changes['resultSet'] && this.resultSet) {
      this.prepareTableColumns();
    }
  }

  private prepareTableColumns(): void {
    this.displayedColumns = [];
    for (const column of this.resultSet.fields) {
      this.displayedColumns.push(column.name);
    }
  }
}
