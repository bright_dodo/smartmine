import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReportService} from '../../_services/report.service';
import {ReportConfig} from '../../_models/report-config';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {UPDATE_SELECTED_REPORT} from '../reports.action';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AssetSearchModalComponent} from '../../lampsman/asset/asset-search-modal/asset-search-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonSearchModalComponent} from '../../lampsman/person/person-search-modal/person-search-modal.component';
import {IAttributeMapEntry} from '../../_models/attribute-map-entry';
import {AttributeTransformPipe} from '../../_pipes/attribute-transform.pipe';
import {ReportResultSet} from '../../_models/report-result-set';
import {ReportOutputContext} from '../../_models/report-output-context';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'sm-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  report: ReportConfig;
  public reportParametersForm: FormGroup;
  reportResults: ReportResultSet[];
  outputContext: ReportOutputContext;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal,
              private reportService: ReportService,
              private formBuilder: FormBuilder,
              private ngRedux: NgRedux<IAppState>) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.report = store.reports.selectedReport;
    });
  }

  ngOnInit() {
    if (!this.report) {
      this.getReportName();
    } else {
      this.initParametersForm();
    }
  }

  private getReportName() {
    let reportName;
    this.route.url.subscribe(response => {
      reportName = this.route.snapshot.params.name;
        this.getReport(reportName);
    });
  }

  private getReport(reportName: any) {
    this.reportService.getReportByName(reportName)
      .subscribe(response => {
        console.log('report ndeiyi', response);
        this.ngRedux.dispatch({type: UPDATE_SELECTED_REPORT, report: response});
        this.initParametersForm();
      });
  }

  private initParametersForm() {
    this.reportParametersForm = this.formBuilder.group({
      parameters: this.parametersFormGroup(),
    });
  }

  executeReport(formValue: any) {
    console.log(formValue);
    const params = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(formValue.parameters);
    console.log('the params we are working with is ', params);
    const data = {name: this.report.name, criteria: params};
    this.reportService.executeReport(data)
      .subscribe(response => {
        console.log('response below');
        console.log(response);
        this.outputContext = response;
        this.reportResults = response.results;
      });
  }

  private parametersFormGroup(): FormGroup {
    const paramsFormGroup = new FormGroup({});
    for (const param of this.report.parameters) {
      paramsFormGroup.addControl(param.name, new FormControl());
    }
    return paramsFormGroup;
  }

  private openAssetSearchModal() {
    const assetSearchModalRef = this.modalService.open(AssetSearchModalComponent, {size: 'lg'});

    assetSearchModalRef.result.then(result => {
      if (result.success) {
        (<FormArray>this.reportParametersForm.controls['parameters']).controls['assetId'].setValue(result.assetId);
      }
    });
  }

  openModal(name: string) {
    if (name === 'assetId') {
      this.openAssetSearchModal();
    } else if (name === 'personId') {
      this.openPersonSearchModal();
    }
  }

  private openPersonSearchModal() {
    const personSearchModalRef = this.modalService.open(PersonSearchModalComponent, {size: 'lg'});

    personSearchModalRef.result.then(result => {
      if (result.success) {
        (<FormArray>this.reportParametersForm.controls['parameters']).controls['personId'].setValue(result.personId);
      }
    });
  }

  getPdf() {
    if (this.outputContext && this.outputContext.resultsPresent) {
      this.reportService.fetchReportPDF(this.outputContext)
        .subscribe((file: Blob) => {
          FileSaver.saveAs(file, this.outputContext.report.name + '.pdf');
        });
    }
  }
}
