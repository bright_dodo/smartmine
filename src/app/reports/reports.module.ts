import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsDashboardComponent } from './reports-dashboard/reports-dashboard.component';
import { LmsReportsComponent } from './lms-reports/lms-reports.component';
import { GdiReportsComponent } from './gdi-reports/gdi-reports.component';
import { AmsReportsComponent } from './ams-reports/ams-reports.component';
import { ScasReportsComponent } from './scas-reports/scas-reports.component';
import { MaintenanceReportsComponent } from './maintenance-reports/maintenance-reports.component';
import {SharedModule} from '../shared/shared.module';
import { AllocationRegisterComponent } from './lms-reports/allocation/allocation-register/allocation-register.component';
import { UnallocatedAssetsReportComponent } from './lms-reports/allocation/unallocated-assets-report/unallocated-assets-report.component';
import { AssetAllocationHistoryReportComponent } from './lms-reports/allocation/asset-allocation-history-report/asset-allocation-history-report.component';
import { PersonAllocationHistoryReportComponent } from './lms-reports/allocation/person-allocation-history-report/person-allocation-history-report.component';
import { AllocationReportsComponent } from './lms-reports/allocation/allocation-reports/allocation-reports.component';
import { RepairReportsComponent } from './lms-reports/repair-reports/repair-reports.component';
import { AssetRepairHistoryComponent } from './lms-reports/repair-reports/asset-repair-history/asset-repair-history.component';
import { StatusChangeComponent } from './lms-reports/repair-reports/status-change/status-change.component';
import { ReportComponent } from './report/report.component';
import { ReportCategoryComponent } from './report-category/report-category.component';
import { ReportDataTableComponent } from './report-data-table/report-data-table.component';
import { MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  declarations: [
    ReportsDashboardComponent,
    LmsReportsComponent,
    GdiReportsComponent,
    AmsReportsComponent,
    ScasReportsComponent,
    MaintenanceReportsComponent,
    AllocationRegisterComponent,
    UnallocatedAssetsReportComponent,
    AssetAllocationHistoryReportComponent,
    PersonAllocationHistoryReportComponent,
    AllocationReportsComponent,
    RepairReportsComponent,
    AssetRepairHistoryComponent,
    StatusChangeComponent,
    ReportComponent,
    ReportCategoryComponent,
    ReportDataTableComponent
  ]
})
export class ReportsModule { }
