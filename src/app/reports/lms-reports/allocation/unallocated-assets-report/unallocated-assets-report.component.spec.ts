import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnallocatedAssetsReportComponent } from './unallocated-assets-report.component';

describe('UnallocatedAssetsReportComponent', () => {
  let component: UnallocatedAssetsReportComponent;
  let fixture: ComponentFixture<UnallocatedAssetsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnallocatedAssetsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnallocatedAssetsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
