import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationRegisterComponent } from './allocation-register.component';

describe('AllocationRegisterComponent', () => {
  let component: AllocationRegisterComponent;
  let fixture: ComponentFixture<AllocationRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocationRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
