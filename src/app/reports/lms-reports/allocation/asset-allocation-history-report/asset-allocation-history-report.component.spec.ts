import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetAllocationHistoryReportComponent } from './asset-allocation-history-report.component';

describe('AssetAllocationHistoryReportComponent', () => {
  let component: AssetAllocationHistoryReportComponent;
  let fixture: ComponentFixture<AssetAllocationHistoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetAllocationHistoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetAllocationHistoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
