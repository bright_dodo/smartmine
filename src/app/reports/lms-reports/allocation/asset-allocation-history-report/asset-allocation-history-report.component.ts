import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {IAppState} from '../../../../app.store';
import {Assignment} from '../../../../_models/assignment';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Person} from '../../../../_models/person';
import {ActivatedRoute} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';
import {Observable, Subject} from 'rxjs';
import {NgRedux} from '@angular-redux/store';
import {Asset} from '../../../../_models/asset';
import {AssetService} from '../../../../_services/asset.service';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'sm-asset-allocation-history-report',
  templateUrl: './asset-allocation-history-report.component.html',
  styleUrls: ['./asset-allocation-history-report.component.css']
})
export class AssetAllocationHistoryReportComponent implements OnInit, AfterViewInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  reportParametersForm: FormGroup;
  showReport = true;
  assets: Person[] = [];
  assignments: Assignment[];
  asset: Asset;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private assetService: AssetService,
              private formBuilder: FormBuilder,
              private ngRedux: NgRedux<IAppState>,
              private route: ActivatedRoute) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.assets = store.lampsman.allPersons;
      this.asset = store.lampsman.selectedAsset;
    });

    this.route.params.subscribe(params => {
      if (params['asset']) {
        this.getAssetByID(params['asset']);
        this.getAssetAssignmentHistory(params['asset']);
      }
    });
  }

  ngOnInit() {
    this.initParamsFormModel();
    if (this.assets.length === 0) {
      // this.fetchAllPersons();
    }

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'pdf'
      ]
    };
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
  }

  generateReport(parameters: any) {
  }

  initParamsFormModel() {
    this.reportParametersForm = this.formBuilder.group({
      assetId: [this.asset !== undefined ? this.asset.assetId : ''],
      assetType: [this.asset !== undefined ? this.asset.assetType.description : ''],
      startDate: [''],
      endDate: [''],
    });
  }

/*  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.assets.filter(v => v.personId.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
  formatter = (x: { personId: string }) => x.personId;*/

  getAssetAssignmentHistory(assetId: string) {
    this.assetService.getAssignmentHistoryForAsset(assetId)
      .subscribe(response => {
        this.assignments = response._embedded['assignmentResources'];
        this.rerender();
      });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  getAssetByID(assetId: string) {
    this.assetService.getAssetByID(assetId)
      .subscribe(asset => {
        this.asset = asset;
      });
  }

}
