import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../../../../_services/person.service';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../../app.store';
import {FETCH_ALL_PERSONS} from '../../../../lampsman/lampsman.action';
import {Person} from '../../../../_models/person';
import {Observable, Subject} from 'rxjs';
import {map, debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Assignment} from '../../../../_models/assignment';
import {ActivatedRoute} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';

@Component({
  selector: 'sm-person-allocation-history-report',
  templateUrl: './person-allocation-history-report.component.html',
  styleUrls: ['./person-allocation-history-report.component.css']
})
export class PersonAllocationHistoryReportComponent implements OnInit, AfterViewInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  reportParametersForm: FormGroup;
  showReport = true;
  persons: Person[] = [];
  assignments: Assignment[];
  person: Person;
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject();
  constructor(private personService: PersonService,
              private formBuilder: FormBuilder,
              private ngRedux: NgRedux<IAppState>,
              private route: ActivatedRoute) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.persons = store.lampsman.allPersons;
    });

    this.route.params.subscribe(params => {
      if (params['person']) {
        this.getPersonById(params['person']);
        this.getPersonAssignmentHistory(params['person']);
      }
    });
  }

  ngOnInit() {
    this.initParamsFormModel();
    if (this.persons.length === 0) {
       this.fetchAllPersons();
    }

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'pdf'
      ]
    };
  }

  ngAfterViewInit() {
    this.dtTrigger.next();
  }

  fetchAllPersons() {
    this.personService.fetchAllPersons()
      .subscribe(response => {
        console.log(response.length);
        this.ngRedux.dispatch({type: FETCH_ALL_PERSONS, persons: response});
      });
  }
  generateReport(parameters: any) {}

  initParamsFormModel() {
    this.reportParametersForm = this.formBuilder.group({
      name: [''],
      companyNo: [''],
      startDate: [''],
      endDate: [''],
    });
  }

/*  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.persons.filter(v => v.personId.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  formatter = (x: {personId: string}) => x.personId;*/

  getPersonById(personId: string) {
    this.personService.getPersonByID(personId)
      .subscribe(person => {
        this.person = person;
      });
  }
  getPersonAssignmentHistory(personId: string) {
    this.personService.getAssignmentHistoryForPerson(personId)
      .subscribe(response => {
        this.assignments = response._embedded['assignmentResources'];
        this.rerender();
      });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
}
