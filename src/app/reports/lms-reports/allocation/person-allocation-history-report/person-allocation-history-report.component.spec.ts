import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAllocationHistoryReportComponent } from './person-allocation-history-report.component';

describe('PersonAllocationHistoryReportComponent', () => {
  let component: PersonAllocationHistoryReportComponent;
  let fixture: ComponentFixture<PersonAllocationHistoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAllocationHistoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAllocationHistoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
