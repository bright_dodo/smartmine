import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetRepairHistoryComponent } from './asset-repair-history.component';

describe('AssetRepairHistoryComponent', () => {
  let component: AssetRepairHistoryComponent;
  let fixture: ComponentFixture<AssetRepairHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetRepairHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetRepairHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
