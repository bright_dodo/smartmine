import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportsDashboardComponent} from './reports-dashboard/reports-dashboard.component';
import {LmsReportsComponent} from './lms-reports/lms-reports.component';
import {GdiReportsComponent} from './gdi-reports/gdi-reports.component';
import {AmsReportsComponent} from './ams-reports/ams-reports.component';
import {ScasReportsComponent} from './scas-reports/scas-reports.component';
import {MaintenanceReportsComponent} from './maintenance-reports/maintenance-reports.component';
import {AllocationReportsComponent} from './lms-reports/allocation/allocation-reports/allocation-reports.component';
import {RepairReportsComponent} from './lms-reports/repair-reports/repair-reports.component';
import {AllocationRegisterComponent} from './lms-reports/allocation/allocation-register/allocation-register.component';
import {UnallocatedAssetsReportComponent} from './lms-reports/allocation/unallocated-assets-report/unallocated-assets-report.component';
import {AssetAllocationHistoryReportComponent} from './lms-reports/allocation/asset-allocation-history-report/asset-allocation-history-report.component';
import {PersonAllocationHistoryReportComponent} from './lms-reports/allocation/person-allocation-history-report/person-allocation-history-report.component';
import {AssetRepairHistoryComponent} from './lms-reports/repair-reports/asset-repair-history/asset-repair-history.component';
import {StatusChangeComponent} from './lms-reports/repair-reports/status-change/status-change.component';
import {ReportComponent} from './report/report.component';
import {ReportCategoryComponent} from './report-category/report-category.component';

const reportRoutes: Routes = [
  {
    path: 'reports',
    component: ReportsDashboardComponent,
    children: [
/*      {
        path: '',
        component: LmsReportsComponent,
        children: [
          {
            path: '',
            component: AllocationReportsComponent,
            children: [
              {
                path: '',
                component: AllocationRegisterComponent
              },
              {
                path: 'lms/allocation/unallocated',
                component: UnallocatedAssetsReportComponent
              },
              {
                path: 'lms/allocation/history/asset',
                component: AssetAllocationHistoryReportComponent
              },
              {
                path: 'lms/allocation/history/person',
                component: PersonAllocationHistoryReportComponent
              }
            ]
          },
          {
            path: 'lms/repair',
            component: RepairReportsComponent,
            children: [
              {
                path: '',
                component: AssetRepairHistoryComponent
              },
              {
                path: 'status',
                component: StatusChangeComponent
              }
            ]
          }
        ]
      },
      {
        path: 'gdi',
        component: GdiReportsComponent,
        children: []
      },
      {
        path: 'ams',
        component: AmsReportsComponent,
        children: []
      },
      {
        path: 'scas',
        component: ScasReportsComponent,
        children: []
      },
      {
        path: 'maintenance',
        component: MaintenanceReportsComponent,
        children: []
      },*/
      {
        path: ':category',
        component: ReportCategoryComponent,
        children: [
          {
            path: ':name',
            component: ReportComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(reportRoutes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
