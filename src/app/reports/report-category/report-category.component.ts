import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ReportService} from '../../_services/report.service';
import {ReportConfig} from '../../_models/report-config';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {UPDATE_SELECTED_REPORT} from '../reports.action';

@Component({
  selector: 'sm-report-category',
  templateUrl: './report-category.component.html',
  styleUrls: ['./report-category.component.css']
})
export class ReportCategoryComponent implements OnInit {
  category: string;
  reports: ReportConfig[];

  constructor(private route: ActivatedRoute,
              private reportService: ReportService,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.getCategory();
  }

  private getReports(category: string) {
    this.reportService.getReportsByCategory(category)
      .subscribe(response => {
        this.reports = response;
      });
  }

  private getCategory() {
    this.route.url.subscribe(response => {
      this.category = this.route.snapshot.params.category;
      this.getReports(this.category);
    });
  }

  selectReport(report: ReportConfig) {
    this.ngRedux.dispatch({type: UPDATE_SELECTED_REPORT, report: report});
  }

}
