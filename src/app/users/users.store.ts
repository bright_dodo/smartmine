import { tassign } from 'tassign';
import {
  LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS, LOGOUT, REGISTRATION_ERROR,
  REGISTRATION_REQUEST, REGISTRATION_SUCCESS
} from './users.actions';
import {IToken} from '../_models/token';
import {User} from '../_models/user';

export interface IUserState {
  token: IToken;
  isLoggingIn: boolean;
  isLoggedIn: boolean;
  isRegistering: boolean;
  newlyRegisteredUser: User;
}

export const USER_INITIAL_STATE: IUserState = {
  token: undefined,
  isLoggingIn: false,
  isLoggedIn: false,
  isRegistering: false,
  newlyRegisteredUser: undefined
};
function loginError(state: IUserState, action: any): IUserState {
  return tassign(state, {isLoggingIn: false});
}

function loginRequest(state: IUserState, action: any): IUserState {
  return tassign(state, {isLoggingIn: true});
}

function loginSuccess(state: IUserState, action: any): IUserState {
  return tassign(state, {isLoggingIn: false, token: action.token, isLoggedIn: true});
}

function logout(state: IUserState, action: any): IUserState {
  return tassign(state, {token: {}, isLoggedIn: false});
}

function registrationError(state: IUserState, action: any): IUserState {
  return tassign(state, {isRegistering: false});
}

function registrationRequest(state: IUserState, action: any): IUserState {
  return tassign(state, {isRegistering: true});
}

function registrationSuccess(state: IUserState, action: any): IUserState {
  return tassign(state, {isRegistering: false, newlyRegisteredUser: action.user});
}

export function userReducer(state: IUserState = USER_INITIAL_STATE, action: any): IUserState {
  switch (action.type) {
    case LOGIN_ERROR:
      return loginError(state, action);
    case LOGIN_REQUEST:
      return loginRequest(state, action);
    case LOGIN_SUCCESS:
      return loginSuccess(state, action);
    case LOGOUT:
      return logout(state, action);
    case REGISTRATION_ERROR:
      return registrationError(state, action);
    case REGISTRATION_REQUEST:
      return registrationRequest(state, action);
    case REGISTRATION_SUCCESS:
      return registrationSuccess(state, action);
    default:
      return state;
  }
}
