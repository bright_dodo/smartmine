import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Assignment} from '../../../_models/assignment';

@Component({
  selector: 'sm-deallocate-confirmation-modal',
  templateUrl: './deallocate-confirmation-modal.component.html',
  styleUrls: ['./deallocate-confirmation-modal.component.css']
})
export class DeallocateConfirmationModalComponent implements OnInit {
  @Input() assignment: Assignment;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  deallocate() {}
}
