import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Asset} from '../../../_models/asset';
import {Subject} from 'rxjs';

@Component({
  selector: 'sm-available-assets-modal',
  templateUrl: './available-assets-modal.component.html',
  styleUrls: ['./available-assets-modal.component.css']
})
export class AvailableAssetsModalComponent implements OnInit, OnChanges {
  @Input() assets: Asset[];
  @Input() assetType: string;
  @Input() assignmentType: string;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(public activeModal: NgbActiveModal) {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
  }

  ngOnInit() {

  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}) {
    if (changes['assets']) {
      console.log(this.assets);
      this.dtTrigger.next();
    }
  }

  allocate(asset: Asset) {
    this.activeModal.close({success: true, assetId: asset.assetId});
  }

}
