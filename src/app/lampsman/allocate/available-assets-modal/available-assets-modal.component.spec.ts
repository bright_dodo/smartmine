import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailableAssetsModalComponent } from './available-assets-modal.component';

describe('AvailableAssetsModalComponent', () => {
  let component: AvailableAssetsModalComponent;
  let fixture: ComponentFixture<AvailableAssetsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableAssetsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailableAssetsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
