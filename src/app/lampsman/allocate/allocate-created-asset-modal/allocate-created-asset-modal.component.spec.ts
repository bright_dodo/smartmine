import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocateCreatedAssetModalComponent } from './allocate-created-asset-modal.component';

describe('AllocateCreatedAssetModalComponent', () => {
  let component: AllocateCreatedAssetModalComponent;
  let fixture: ComponentFixture<AllocateCreatedAssetModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocateCreatedAssetModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocateCreatedAssetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
