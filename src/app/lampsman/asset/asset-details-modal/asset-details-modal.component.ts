import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Asset} from '../../../_models/asset';
import {Assignment} from '../../../_models/assignment';

@Component({
  selector: 'sm-asset-details-modal',
  templateUrl: './asset-details-modal.component.html',
  styleUrls: ['./asset-details-modal.component.css']
})
export class AssetDetailsModalComponent implements OnInit {
  @Input() asset: Asset;
  assignment: Assignment;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
}
