import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Assignment} from '../../../_models/assignment';
import {Asset} from '../../../_models/asset';

@Component({
  selector: 'sm-asset-assignment-history-modal',
  templateUrl: './asset-assignment-history-modal.component.html',
  styleUrls: ['./asset-assignment-history-modal.component.css']
})
export class AssetAssignmentHistoryModalComponent implements OnInit {
  @Input() asset: Asset;
  @Input() assignments: Assignment[];
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
