import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetAssignmentHistoryModalComponent } from './asset-assignment-history-modal.component';

describe('AssetAssignmentHistoryModalComponent', () => {
  let component: AssetAssignmentHistoryModalComponent;
  let fixture: ComponentFixture<AssetAssignmentHistoryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetAssignmentHistoryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetAssignmentHistoryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
