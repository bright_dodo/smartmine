import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSearchModalComponent } from './asset-search-modal.component';

describe('AssetSearchModalComponent', () => {
  let component: AssetSearchModalComponent;
  let fixture: ComponentFixture<AssetSearchModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSearchModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSearchModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
