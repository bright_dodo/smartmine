import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Person} from '../../../_models/person';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../../../_services/person.service';
import {IAttributeMapEntry} from '../../../_models/attribute-map-entry';
import {AttributeTransformPipe} from '../../../_pipes/attribute-transform.pipe';
import {ToastrService} from 'ngx-toastr';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {UPDATE_SELECTED_PERSON} from '../../lampsman.action';

@Component({
  selector: 'sm-modify-person-modal',
  templateUrl: './modify-person-modal.component.html',
  styleUrls: ['./modify-person-modal.component.css']
})
export class ModifyPersonModalComponent implements OnInit {
  @Input() person: Person;
  editPersonForm: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: FormBuilder,
              private personService: PersonService,
              private toastr: ToastrService,
              private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
    this.initFormModel();
  }

  attributeMapFormGroup(): FormGroup {
    const attribFormGroup: FormGroup = new FormGroup({});
    for (const attrib of this.person.attr) {
      attribFormGroup.addControl(attrib.name, new FormControl(attrib.value));
    }
    return attribFormGroup;
  }

  initFormModel() {
    this.editPersonForm = this.formBuilder.group({
      personId: [this.person.personId, [Validators.required]],
      type: [this.person.type],
      attributeMap: this.attributeMapFormGroup()
    });
  }

  modifyPerson(form: any) {
    const map = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(form.attributeMap);
    form.attributeMap = map;
    this.personService.editPerson(form)
      .subscribe(
      response => {
        this.toastr.success('Person' + response.personId + ' was successfully modified!', 'Person Edited');
        this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON, selectedPerson: response});
      },
      error2 => {
        this.toastr.error('The person could not be modified due to an error', 'Error');
      }
    ),
      () => {
      this.activeModal.close();
      };
  }

  selectPhoto() {}
  addInduction() {}
  addMedical() {}
  addTraining() {}
}
