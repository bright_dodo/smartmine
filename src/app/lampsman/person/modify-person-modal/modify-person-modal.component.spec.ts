import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyPersonModalComponent } from './modify-person-modal.component';

describe('ModifyPersonModalComponent', () => {
  let component: ModifyPersonModalComponent;
  let fixture: ComponentFixture<ModifyPersonModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyPersonModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyPersonModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
