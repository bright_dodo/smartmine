import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Person} from '../../../_models/person';

@Component({
  selector: 'sm-person-details-modal',
  templateUrl: './person-details-modal.component.html',
  styleUrls: ['./person-details-modal.component.css']
})
export class PersonDetailsModalComponent implements OnInit {
  @Input() person: Person;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
