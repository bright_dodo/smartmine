import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {IPersonType} from '../../../_models/person-type';
import {NgRedux, select} from '@angular-redux/store';
import {Person} from '../../../_models/person';
import {Observable} from 'rxjs/index';
import {PersonService} from '../../../_services/person.service';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {
  SHOW_PERSONSEARCH_RESULTS,
  UPDATE_PERSON_INFORMATION_FOCUS,
  UPDATE_PERSONSEARCH_RESULTS,
  UPDATE_SELECTED_PERSON
} from '../../lampsman.action';
import {MatPaginator, MatSort} from '@angular/material';
import {PersonSearchResultsDataSource} from '../../search/search-person/person-search-results/person-search-results-datasource';

@Component({
  selector: 'sm-person-search-modal',
  templateUrl: './person-search-modal.component.html',
  styleUrls: ['./person-search-modal.component.css']
})
export class PersonSearchModalComponent implements OnInit {
  showSearchResults = false;
  simpleView = true;
  advancedView = false;

  @select(s => s.lampsman.personSearchResults) searchResults: Observable<Person[]>;
  @select(s => s.lampsman.personTypes) personTypes: Observable<IPersonType>;

  searchPersonForm: FormGroup;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: PersonSearchResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['personId', 'names', 'shaft', 'shift'];

  constructor(public activeModal: NgbActiveModal,
              private personService: PersonService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.personService.fetchPersonTypes();
    this.initFormModel();
    this.dataSource = new PersonSearchResultsDataSource(this.paginator, this.sort);
  }

  initFormModel() {
    this.searchPersonForm = new FormGroup({
      name: new FormControl(),
      personType: new FormControl(),
      companyNo: new FormControl(),
      identityNo: new FormControl()
    });
  }

  searchPerson(form: any) {
    this.personService.searchPerson(form)
      .subscribe(response => {
          const result = response._embedded['personResources'];
          if (result && result.length > 0) {
            this.ngRedux.dispatch({type: UPDATE_PERSONSEARCH_RESULTS, personSearchResults: result});
            this.ngRedux.dispatch({type: SHOW_PERSONSEARCH_RESULTS, show: true});
          }
        },
        error2 => {
          if (error2.error.code === 404) {
            this.toastr.warning(error2.error.message, 'Person Not Found');
          } else {
            this.toastr.error('There was an error searching person!', 'Search Error ' + error2.error.status);
          }
        });
  }

  toggleSimpleAdvanced() {
    this.simpleView = !this.simpleView;
    this.advancedView = !this.advancedView;
  }

  selectPerson(person: Person) {
    this.activeModal.close({success: true, personId: person.personId});
  }

}
