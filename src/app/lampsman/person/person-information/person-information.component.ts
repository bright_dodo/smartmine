import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PersonDetailsModalComponent} from '../../person/person-details-modal/person-details-modal.component';
import {ModifyPersonModalComponent} from '../../person/modify-person-modal/modify-person-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'sm-person-information',
  templateUrl: './person-information.component.html',
  styleUrls: ['./person-information.component.css']
})
export class PersonInformationComponent implements OnInit {
  person: Person;
  personAssignments: Assignment[];
  focus: boolean;

  constructor(private ngRedux: NgRedux<IAppState>,
              private modalService: NgbModal,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
      this.personAssignments = store.lampsman.personAssignments;
      this.focus = store.lampsman.personInformationFocus;
    });
  }

  ngOnInit() {
  }

  allocate() {

  }
  viewDetails() {
    const detailsModalRef = this.modalService.open(PersonDetailsModalComponent);
    detailsModalRef.componentInstance.person = this.person;
  }

  modify() {
    const modifyModalRef = this.modalService.open(ModifyPersonModalComponent);
    modifyModalRef.componentInstance.person = this.person;
  }

  deallocate(assignment: any) {}

  repairHistory(asset: any) {}

  allocationHistory(person: any) {
    this.router.navigate(['/reports/lms/allocation/history/person', {person: this.person.personId}]);
  }
}
