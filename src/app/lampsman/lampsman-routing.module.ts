import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LampsmanDashboardComponent} from './lampsman-dashboard/lampsman-dashboard.component';
import {SearchComponent} from './search/search.component';
import {CreateComponent} from './create/create.component';

const lampsmanRoutes: Routes = [
  { path: 'lampsman',
    component: LampsmanDashboardComponent,
    children: [
      {
        path: '',
        component: SearchComponent
      },
      {
        path: 'create',
        component: CreateComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(lampsmanRoutes)],
  exports: [RouterModule]
})
export class LampsmanRoutingModule { }
