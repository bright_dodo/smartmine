import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable, Subject} from 'rxjs';
import {IAssetType} from '../../../_models/asset-type';
import {FormControl, FormGroup} from '@angular/forms';
import {AssetService} from '../../../_services/asset.service';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {
  ADD_RECENTLY_ALLOCATED_ASSET,
  ADD_RECENTLY_DEALLOCATED_ASSET,
  SEARCH_ASSET_SUCCESS,
  SHOW_PERSONSEARCH_RESULTS,
  UPDATE_ACTIVE_TAG_READER_LIST, UPDATE_ASSET_SEARCH_RESULTS,
  UPDATE_PASSIVE_TAG_READER_LIST,
  UPDATE_PERSON_ASSIGNMENTS,
  UPDATE_PERSON_INFORMATION_FOCUS, UPDATE_SELECTED_ASSET,
  UPDATE_SELECTED_PASSIVE_TAG_READER,
  UPDATE_SELECTED_PERSON,
} from '../../lampsman.action';
import {IAttribute} from '../../../_models/attribute';
import {Asset} from '../../../_models/asset';
import {Person} from '../../../_models/person';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AssetDetailsModalComponent} from '../../asset/asset-details-modal/asset-details-modal.component';
import {ModifyAssetModalComponent} from '../../asset/modify-asset-modal/modify-asset-modal.component';
import {Router} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';
import {DeallocateConfirmationModalComponent} from '../../allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';
import {Assignment} from '../../../_models/assignment';
import {Location} from '../../../_models/location';
import {LocationService} from '../../../_services/location.service';
import {forEach} from '@angular/router/src/utils/collection';
import {PersonService} from '../../../_services/person.service';
import {LookupService} from '../../../_services/lookup.service';

@Component({
  selector: 'sm-search-asset',
  templateUrl: './search-asset.component.html',
  styleUrls: ['./search-asset.component.css']
})
export class SearchAssetComponent implements OnInit, AfterViewInit {
  simpleView = true;
  advancedView = false;
  @select(s => s.lampsman.assetTypes) assetTypes: Observable<IAssetType>;
  assetTypeAttributes: IAttribute[];
  @select(s => s.lampsman.assetSearchResults) assetSearchResults: Observable<Asset[]>;
  assets: Asset[];
  passiveTagReaders: Location[];
  activeTagReaders: Location[];
  selectedPassiveTagReader: Location;
  selectedActiveTagReader: Location;
  maxResultsOptions = [5, 10, 25, 50, 100, 200];
  statusOptions: any[];
  shiftOptions: any[];
  activeTagReaderLocationOptions: any[];

  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTriggerAsset: Subject<any> = new Subject();
  person: Person;
  searchAssetForm: FormGroup;

  constructor(private assetService: AssetService,
              private locationService: LocationService,
              private personService: PersonService,
              private lookupService: LookupService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private modalService: NgbModal,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
      this.passiveTagReaders = store.lampsman.passiveTagReaderList;
      this.activeTagReaders = store.lampsman.activeTagReaderList;
      this.selectedActiveTagReader = store.lampsman.selectedActiveTagReader;
      this.selectedPassiveTagReader = store.lampsman.selectedPassiveTagReader;
    });
  }

  ngOnInit() {
    this.assetService.fetchAssetTypes();
    this.initFormModel();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.fetchPassiveTagReaders();
    this.fetchActiveTagReaders();
    this.fetchStatusOptions();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.dtTriggerAsset.next();
    });
  }

  toggleSimpleAdvanced() {
    this.simpleView = !this.simpleView;
    this.advancedView = !this.advancedView;
  }

  initFormModel() {
    this.searchAssetForm = new FormGroup({
      assetId: new FormControl(),
      assetType: new FormControl(),
      attributeName: new FormControl(),
      attributeValue: new FormControl(),
      maxResults: new FormControl(),
      status: new FormControl(),
      spareIndicator: new FormControl(),
      tracked: new FormControl(),
      allocated: new FormControl(),
      tagged: new FormControl(),
      shift: new FormControl()
    });
  }

  onTypeChanged(type: any) {
    this.assetService.getAssetTypeAttributes(type)
      .subscribe(response => {
        this.assetTypeAttributes = response;
      });
  }

  passiveTagReaderLocationChanged(readerName: any) {
    for (const loc of this.passiveTagReaders) {
      if (loc.name === readerName) {
        this.ngRedux.dispatch({type: UPDATE_SELECTED_PASSIVE_TAG_READER, reader: loc});
      }
    }
  }

  searchAsset(formValue: any) {
    if (!formValue.maxResults) {
      formValue.maxResults = 100;
    }
    this.assetService.searchAsset(formValue)
      .subscribe(response => {
          if (response._embedded !== undefined) {
            const result = response._embedded['assetResources'];
            if (result.length > 0) {
              this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: result});
            }
          } else {
            this.toastr.info('No asset matched your search criteria', 'Asset not found');
            this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: []});
          }
        },
        error2 => {
          console.log(error2);
          this.toastr.error('There was an error searching for Assets!', 'Search Error');
        });
  }

  allocate(asset: Asset) {
    if (this.person) {
      const ass = {assetId: asset.assetId, personId: this.person.personId, status: 'allocated'};
      this.assetService.assignAsset(ass)
        .subscribe(response => {
          const newAssignments = response._embedded['assignmentResources'];
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: newAssignments});
          this.toastr.success('Asset was allocated successfully', 'Allocation Successful!');
          this.ngRedux.dispatch({type: ADD_RECENTLY_ALLOCATED_ASSET, asset: asset});
          const updatedAsset = newAssignments.find(x => x.asset.assetId === asset.assetId);
          console.log(updatedAsset);
          this.ngRedux.dispatch({type: UPDATE_ASSET_SEARCH_RESULTS, asset: updatedAsset.asset});
        });
    } else {
      this.toastr.warning('To assign an asset, please choose a person to assign to first', 'No Person Selected');
    }
  }

  viewDetails(asset: Asset) {
    // const assign: Assignment;
    if (asset.allocStatus === 'allocated' || asset.allocStatus === 'temp-alloc') {
      /*      asset.getRelation(Assignment, 'assignment').subscribe(assignment => {
              assign = assignment;
            });*/
    }
    const detailModalRef = this.modalService.open(AssetDetailsModalComponent);
    detailModalRef.componentInstance.asset = asset;
    /*    if (assign !== undefined) {
          detailModalRef.componentInstance.assignment = assign;
        }*/
  }

  modify(asset: Asset) {
    const modifyModalRef = this.modalService.open((ModifyAssetModalComponent));
    modifyModalRef.componentInstance.asset = asset;
  }

  repairHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/repair', {asset: asset.assetId}]);
  }

  allocationHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/allocation/history/asset', {asset: asset.assetId}]);
  }

  reRenderDatatable() {
    this.dtElement.dtInstance.then((tableInstance: DataTables.Api) => {
      // Destroy the table first
      tableInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTriggerAsset.next();
    });
  }

  fetchActiveTagReaders() {
    this.locationService.getLocationsByType('snowball-bay')
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_ACTIVE_TAG_READER_LIST, readers: response});
      });
  }

  fetchPassiveTagReaders() {
    this.locationService.getLocationsByType('allocation-bay')
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_PASSIVE_TAG_READER_LIST, readers: response});
      });
  }

  scanPassiveTags() {
    if (!this.selectedPassiveTagReader) {
      this.toastr.warning('Please select a valid Tag Reader Location!', 'Reader Not Selected');
    } else {
      this.locationService.getListOfTagsAtLocation(this.selectedPassiveTagReader.name)
        .subscribe(response => {
            const assets: Asset[] = [];
            assets.push(response);
            this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: assets});
            this.getPersonForAsset(response);
          },
          error2 => {
            this.toastr.error(error2.error.message, 'Error: ' + error2.error.status);
          });
    }
  }

  getPersonForAsset(asset: Asset) {
    this.assetService.getPersonForAsset(asset.assetId)
      .subscribe(response => {
        if (response) {
          this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON, selectedPerson: response});
          this.ngRedux.dispatch({type: UPDATE_PERSON_INFORMATION_FOCUS, focus: true});
          this.fetchPersonAssignments(response);
        }
      });
  }

  fetchPersonAssignments(person: Person) {
    this.personService.getAssignmentsForPerson(person.personId)
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
      });
  }

  private fetchStatusOptions() {
    this.lookupService.getLookupList('assetStatus')
      .subscribe(response => {
        this.statusOptions = response;
      });
  }
}
