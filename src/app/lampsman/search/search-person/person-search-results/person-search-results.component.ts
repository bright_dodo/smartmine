import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {PersonSearchResultsDataSource} from './person-search-results-datasource';
import {
  SHOW_PERSONSEARCH_RESULTS,
  UPDATE_PERSON_ASSIGNMENTS,
  UPDATE_PERSON_INFORMATION_FOCUS,
  UPDATE_SELECTED_PERSON
} from '../../../lampsman.action';
import {Person} from '../../../../_models/person';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {PersonService} from '../../../../_services/person.service';

@Component({
  selector: 'sm-person-search-results',
  templateUrl: './person-search-results.component.html',
  styleUrls: ['./person-search-results.component.css']
})
export class PersonSearchResultsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: PersonSearchResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['personId', 'names', 'shaft', 'shift'];

  constructor(private personService: PersonService,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.dataSource = new PersonSearchResultsDataSource(this.paginator, this.sort);
  }

  selectPerson(person: Person) {
    this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON, selectedPerson: person});
    this.ngRedux.dispatch({type: SHOW_PERSONSEARCH_RESULTS, show: false});
    this.ngRedux.dispatch({type: UPDATE_PERSON_INFORMATION_FOCUS, focus: true});
    this.fetchPersonAssignments(person);
  }

  fetchPersonAssignments(person: Person) {
    this.personService.getAssignmentsForPerson(person.personId)
      .subscribe(response => {
        if (response._embedded) {
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
        } else {
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: []});
        }
      });
  }
}
