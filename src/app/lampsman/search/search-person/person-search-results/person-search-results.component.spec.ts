
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSearchResultsComponent } from './person-search-results.component';

describe('PersonSearchResultsComponent', () => {
  let component: PersonSearchResultsComponent;
  let fixture: ComponentFixture<PersonSearchResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonSearchResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PersonSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
