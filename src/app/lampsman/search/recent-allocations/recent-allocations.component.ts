import { Component, OnInit } from '@angular/core';
import {Asset} from '../../../_models/asset';
import {Observable} from 'rxjs';
import {NgRedux, select} from '@angular-redux/store';
import {AssetDetailsModalComponent} from '../../asset/asset-details-modal/asset-details-modal.component';
import {ModifyAssetModalComponent} from '../../asset/modify-asset-modal/modify-asset-modal.component';
import {Assignment} from '../../../_models/assignment';
import {DeallocateConfirmationModalComponent} from '../../allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AssetService} from '../../../_services/asset.service';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {
  ADD_RECENTLY_ALLOCATED_ASSET, ADD_RECENTLY_DEALLOCATED_ASSET, REMOVE_RECENTLY_ALLOCATED_ASSET, REMOVE_RECENTLY_DEALLOCATED_ASSET,
  UPDATE_PERSON_ASSIGNMENTS
} from '../../lampsman.action';
import {Router} from '@angular/router';
import {Person} from '../../../_models/person';

@Component({
  selector: 'sm-recent-allocations',
  templateUrl: './recent-allocations.component.html',
  styleUrls: ['./recent-allocations.component.css']
})
export class RecentAllocationsComponent implements OnInit {
  @select(s => s.lampsman.recentlyAllocatedAssets) allocations: Observable<Asset[]>;
  @select(s => s.lampsman.recentlyDeallocatedAssets) deallocations: Observable<Asset[]>;
  person: Person;
  dtOptions: DataTables.Settings = {};

  constructor(private modalService: NgbModal,
              private assetService: AssetService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
    });
  }

  ngOnInit() {
  }

  viewDetails(asset: Asset) {
    const detailModalRef = this.modalService.open(AssetDetailsModalComponent);
    detailModalRef.componentInstance.asset = asset;
  }

  modify(asset: Asset) {
    const modifyModalRef = this.modalService.open((ModifyAssetModalComponent));
    modifyModalRef.componentInstance.asset = asset;
  }

  deallocate(asset: Asset) {
    this.findAssetAssignment(asset);
  }

  allocate(asset: Asset) {
    if (this.person) {
      const ass = {assetId: asset.assetId, personId: this.person.personId, status: 'allocated'};
      this.assetService.assignAsset(ass)
        .subscribe(response => {
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
          this.toastr.success('Asset was allocated successfully', 'Allocation Successful!');
          this.ngRedux.dispatch({type: REMOVE_RECENTLY_DEALLOCATED_ASSET, asset: asset});
          this.ngRedux.dispatch({type: ADD_RECENTLY_ALLOCATED_ASSET, asset: asset});
        });
    }else {
      this.toastr.warning('To assign an asset, please choose a person to assign to first', 'No Person Selected');
    }
  }

  repairHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/repair', {asset: asset.assetId}]);
  }

  allocationHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/allocation/history/asset', {asset: asset.assetId}]);
  }

  findAssetAssignment(asset: Asset): Assignment {
    let assignment: Assignment;
    this.assetService.findAssignmentForAsset(asset.assetId)
      .subscribe(response => {
        assignment = response;
        this.openDeassignConfirmationModal(asset, assignment);
      });
    return assignment;
  }

  openDeassignConfirmationModal(asset: Asset, assignment: Assignment) {
    const deallocateModalRef = this.modalService.open(DeallocateConfirmationModalComponent);
    deallocateModalRef.componentInstance.assignment = assignment;
    deallocateModalRef.result.then(result => {
      if (result) {
        const ass = {id: assignment.assignmentId, personId: assignment.person.personId};
        this.assetService.deassignAsset(ass)
          .subscribe(response => {
            this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response});
            this.toastr.info('The asset has been deallocated', 'Asset Deassigned');
            this.ngRedux.dispatch({type: REMOVE_RECENTLY_ALLOCATED_ASSET, asset: asset});
            this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: asset});
          });
      }
    });
  }
}
