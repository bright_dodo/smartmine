import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyCreatedAssetsComponent } from './recently-created-assets.component';

describe('RecentlyCreatedAssetsComponent', () => {
  let component: RecentlyCreatedAssetsComponent;
  let fixture: ComponentFixture<RecentlyCreatedAssetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentlyCreatedAssetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyCreatedAssetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
