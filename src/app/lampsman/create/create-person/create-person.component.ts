import {Component, Input, OnInit} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {IPersonType} from '../../../_models/person-type';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PersonService} from '../../../_services/person.service';
import {IAppState} from '../../../app.store';
import {Person} from '../../../_models/person';
import {AttributeTransformPipe} from '../../../_pipes/attribute-transform.pipe';
import {IAttributeMapEntry} from '../../../_models/attribute-map-entry';
import {ADD_RECENTLY_CREATED_PERSON} from '../../lampsman.action';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'sm-create-person',
  templateUrl: './create-person.component.html',
  styleUrls: ['./create-person.component.css']
})
export class CreatePersonComponent implements OnInit {
  @select(s => s.lampsman.showPersonElements) showCard: Observable<boolean>;
  @Input() personType: IPersonType;
  createPersonForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private personService: PersonService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.initFormModel();
  }

  attributeMapFormGroup(): FormGroup {
    const attribFormGroup: FormGroup = new FormGroup({});
    for (const attrib of this.personType.attributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  initFormModel() {
    this.createPersonForm = this.formBuilder.group({
      personId: ['', [Validators.required]],
      type: [this.personType],
      attributeMap: this.attributeMapFormGroup()
    });
  }

  createPerson(form: any) {
    const map = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(form.attributeMap);
    form.attributeMap = map;
    form.type = this.personType.name;
    this.personService.createPerson(form).subscribe(
      response => {
        this.toastr.success('New person ' + response.personId + ' was successfully created!', 'Person Created');
        this.ngRedux.dispatch({type: ADD_RECENTLY_CREATED_PERSON, person: response});
      },
      error2 => {
        this.toastr.error('The new person could not be created due to an error', 'Error');
      }
    );
  }

  selectPhoto() {}
  addInduction() {}
  addMedical() {}
  addTraining() {}
}
