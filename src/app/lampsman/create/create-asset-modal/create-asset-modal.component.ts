import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {IAssetType} from '../../../_models/asset-type';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IAttributeMapEntry} from '../../../_models/attribute-map-entry';
import {AttributeTransformPipe} from '../../../_pipes/attribute-transform.pipe';
import {AssetService} from '../../../_services/asset.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'sm-create-asset-modal',
  templateUrl: './create-asset-modal.component.html',
  styleUrls: ['./create-asset-modal.component.css']
})
export class CreateAssetModalComponent implements OnInit {
  @Input() assetType: IAssetType;
  public createAssetForm: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private ngRedux: NgRedux<IAppState>,
              private formBuilder: FormBuilder,
              private assetService: AssetService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.initFormModel();
  }

  initFormModel(): void {
    this.createAssetForm = this.formBuilder.group({
      assetId: ['', [Validators.required]],
      type: [this.assetType],
      status: ['operational'],
      allocStatus: ['unallocated'],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  attributeMapFormGroup(): FormGroup {
    const attribFormGroup = new FormGroup({});
    for (const attrib of this.assetType.attributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  createAsset(form: any) {
    const map: any[] = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(form.attributeMap);
    form.attributeMap = map;
    form.type = this.assetType.name;
    this.assetService.createAsset(form)
      .subscribe(response => {
        this.activeModal.close({success: true, asset: response});
        this.toastr.success('Successfully created a new ' + this.assetType.description, 'Success');
      },
        error2 => {
        this.toastr.error('Sorry, new asset could not be created', 'Error');
        this.activeModal.close({success: false, error: error2});
        });
  }
}
