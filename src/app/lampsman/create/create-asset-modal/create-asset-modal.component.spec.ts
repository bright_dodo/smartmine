import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAssetModalComponent } from './create-asset-modal.component';

describe('CreateAssetModalComponent', () => {
  let component: CreateAssetModalComponent;
  let fixture: ComponentFixture<CreateAssetModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAssetModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAssetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
