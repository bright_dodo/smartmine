import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {select} from '@angular-redux/store';
import {Person} from '../../../_models/person';

@Component({
  selector: 'sm-recently-created-persons',
  templateUrl: './recently-created-persons.component.html',
  styleUrls: ['./recently-created-persons.component.css']
})
export class RecentlyCreatedPersonsComponent implements OnInit {
  @select(s => s.lampsman.showPersonElements) showCard: Observable<boolean>;
  @select(s => s.lampsman.recentlyCreatedPersons) persons: Observable<Person[]>;
  constructor() { }

  ngOnInit() {
  }

}
