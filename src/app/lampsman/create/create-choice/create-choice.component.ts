import { Component, OnInit } from '@angular/core';
import {AssetService} from '../../../_services/asset.service';
import {PersonService} from '../../../_services/person.service';
import {NgRedux, select} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {IAssetType} from '../../../_models/asset-type';
import {IPersonType} from '../../../_models/person-type';
import {
  DISPLAY_ASSET_ELEMENTS, DISPLAY_PERSON_ELEMENTS,
  UPDATE_SELECTED_PERSON_TYPE
} from '../../lampsman.action';
import {Observable} from 'rxjs';

@Component({
  selector: 'sm-create-choice',
  templateUrl: './create-choice.component.html',
  styleUrls: ['./create-choice.component.css']
})
export class CreateChoiceComponent implements OnInit {
/*  showEquipmentElements = true;
  showPersonElements = false;
  @select(s => s.lampsman.assetTypes) equipmentTypes: Observable<IAssetType[]>;
  @select(s => s.lampsman.personTypes) personTypes: Observable<IPersonType[]>;
  selectedEquipmentType: IAssetType = null;*/

  constructor(private assetService: AssetService,
              private personService: PersonService,
              private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
    console.log('zvii zvirikuitika?');
    this.assetService.fetchAssetTypes();
    this.personService.fetchPersonTypes();
  }

/*  changeTab(choice: any) {
    if (choice === 'person') {
      this.showPersonElements = true;
      this.showEquipmentElements = false;
    }
    if (choice === 'equipment') {
      this.showPersonElements = false;
      this.showEquipmentElements = true;
    }
    this.ngRedux.dispatch({type: DISPLAY_ASSET_ELEMENTS, show: this.showEquipmentElements});
    this.ngRedux.dispatch({type: DISPLAY_PERSON_ELEMENTS, show: this.showPersonElements});
  }

  equipmentTypeSelected(type: any) {
    console.log(type);
    this.selectedEquipmentType = type;
    // this.assetService.getAssetTypeAttributes(type);
  }

  personTypeSelected(type: any) {
    this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON_TYPE, personType: type});
    // this.personService.getPersonTypeAttributes(type);
  }*/


}
