import { Component, OnInit } from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {IAppState} from '../../app.store';
import {IAssetType} from '../../_models/asset-type';
import {IPersonType} from '../../_models/person-type';

@Component({
  selector: 'sm-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  @select(s => s.lampsman.showPersonCreateForm) showPersonForm: Observable<boolean>;
  @select(s => s.lampsman.showAssetCreateForm) showAssetForm: Observable<boolean>;
  assetType: IAssetType;
  personType: IPersonType;

  constructor(private ngRedux: NgRedux<IAppState>) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.assetType = store.lampsman.selectedAssetType;
      this.personType = store.lampsman.selectedPersonType;
    });
  }

  ngOnInit() {
  }

}
