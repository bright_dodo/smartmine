import {Component, Input, OnChanges, OnDestroy, OnInit} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {IAttributeMapEntry} from '../../../_models/attribute-map-entry';
import {AttributeTransformPipe} from '../../../_pipes/attribute-transform.pipe';
import {ToastrService} from 'ngx-toastr';
import {AssetService} from '../../../_services/asset.service';
import {IAppState} from '../../../app.store';
import {IAssetType} from '../../../_models/asset-type';
import {ADD_RECENTLY_CREATED_ASSET} from '../../lampsman.action';

@Component({
  selector: 'sm-create-asset',
  templateUrl: './create-asset.component.html',
  styleUrls: ['./create-asset.component.css']
})
export class CreateAssetComponent implements OnInit, OnChanges {
  @select(s => s.lampsman.showAssetElements) showCard: Observable<boolean>;
  @Input() assetType: IAssetType;
  public createAssetForm: FormGroup;

  constructor(private ngRedux: NgRedux<IAppState>,
              private formBuilder: FormBuilder,
              private assetService: AssetService,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.initFormModel();
  }

  ngOnChanges() {
    this.initFormModel();
  }

  initFormModel(): void {
    this.createAssetForm = this.formBuilder.group({
      assetId: ['', [Validators.required]],
      type: [this.assetType],
      status: ['operational'],
      allocStatus: ['unallocated'],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  attributeMapFormGroup(): FormGroup {
    const attribFormGroup = new FormGroup({});
    for (const attrib of this.assetType.attributes) {
      attribFormGroup.addControl(attrib.name, new FormControl());
    }
    return attribFormGroup;
  }

  createAsset(model: any) {
    console.log(model);
    const map: any[] = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(model.attributeMap);
    model.attributeMap = map;
    model.type = this.assetType.name;
    console.log(model);
    this.assetService.createAsset(model)
      .subscribe(response => {
        this.ngRedux.dispatch({type: ADD_RECENTLY_CREATED_ASSET, asset: response});
          this.toastr.success('Successfully created a new ' + this.assetType.description, 'Success');
        },
        error2 => {
          this.toastr.error('Sorry, new asset could not be created', 'Error');
        });
  }

}
