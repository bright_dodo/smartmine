import { Component, OnInit } from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {IAssetType} from '../../../_models/asset-type';
import {IPersonType} from '../../../_models/person-type';
import {AssetService} from '../../../_services/asset.service';
import {IAppState} from '../../../app.store';
import {PersonService} from '../../../_services/person.service';
import {
  DISPLAY_ASSET_ELEMENTS, DISPLAY_PERSON_ELEMENTS, SHOW_ASSET_CREATE_FORM, SHOW_PERSON_CREATE_FORM, UPDATE_SELECTED_ASSET_TYPE,
  UPDATE_SELECTED_PERSON_TYPE
} from '../../lampsman.action';
import {Router} from '@angular/router';

@Component({
  selector: 'sm-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css']
})
export class ChoiceComponent implements OnInit {
  showEquipmentElements = true;
  showPersonElements = false;
  assetType: any;
  @select(s => s.lampsman.assetTypes) assetTypes: Observable<IAssetType[]>;
  @select(s => s.lampsman.personTypes) personTypes: Observable<IPersonType[]>;

  constructor(private ngRedux: NgRedux<IAppState>,
              private personService: PersonService,
              private assetService: AssetService,
              private router: Router) { }

  ngOnInit() {
    this.assetService.fetchAssetTypes();
    this.personService.fetchPersonTypes();
    this.ngRedux.dispatch({type: DISPLAY_ASSET_ELEMENTS, show: this.showEquipmentElements});
    this.ngRedux.dispatch({type: DISPLAY_PERSON_ELEMENTS, show: this.showPersonElements});
  }

  changeTab(choice: any) {
    if (choice === 'person') {
      this.showPersonElements = true;
      this.showEquipmentElements = false;
    }
    if (choice === 'equipment') {
      this.showPersonElements = false;
      this.showEquipmentElements = true;
    }
    this.ngRedux.dispatch({type: DISPLAY_ASSET_ELEMENTS, show: this.showEquipmentElements});
    this.ngRedux.dispatch({type: DISPLAY_PERSON_ELEMENTS, show: this.showPersonElements});
  }

  assetTypeSelected(type?: any) {
    this.assetService.getAssetTypeByName(type)
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_SELECTED_ASSET_TYPE, assetType: response});
        this.ngRedux.dispatch({type: SHOW_ASSET_CREATE_FORM, show: true});
      });
  }

  personTypeSelected(type: any) {
    this.personService.getPersonTypeByName(type)
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON_TYPE, personType: response});
        this.ngRedux.dispatch({type: SHOW_PERSON_CREATE_FORM, show: true});
      });
  }
}
