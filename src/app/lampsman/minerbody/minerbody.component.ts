import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {ToastrService} from 'ngx-toastr';
import {PersonService} from '../../_services/person.service';
import {Person} from '../../_models/person';
import {Assignment} from '../../_models/assignment';
import {CLEAR_SEARCH} from '../lampsman.action';

@Component({
  selector: 'sm-minerbody',
  templateUrl: './minerbody.component.html',
  styleUrls: ['./minerbody.component.css']
})
export class MinerbodyComponent implements OnInit {
  person: Person;
  assignments: Assignment[] = [];
  assignedCaplamps: Assignment[] = [];
  assignedGdis: Assignment[] = [];
  assignedRescuePacks: Assignment[] = [];
  assignedPds: Assignment[] = [];
  assignedMisc: Assignment[] = [];

  constructor(private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private personService: PersonService) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
      this.assignments = store.lampsman.personAssignments;
      this.processAssignments(this.assignments);
    });
  }

  ngOnInit() {
  }

  processAssignments(assignments: Assignment[]): void {
    this.assignedCaplamps = [];
    this.assignedGdis = [];
    this.assignedRescuePacks = [];
    for (const assignment of assignments) {
      if (assignment.asset.type.assetClass.name === 'cap-lamp') {
        this.assignedCaplamps.push(assignment);
      } else if (assignment.asset.type.assetClass.name === 'gas-detector') {
        this.assignedGdis.push(assignment);
      } else if (assignment.asset.type.assetClass.name === 'rescue-pack') {
        this.assignedRescuePacks.push(assignment);
      } else {
        this.assignedMisc.push(assignment);
      }
    }
  }

  clearSearch() {
    this.ngRedux.dispatch({type: CLEAR_SEARCH});
  }

}
