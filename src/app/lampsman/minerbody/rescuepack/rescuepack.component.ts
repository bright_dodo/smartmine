import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {ToastrService} from 'ngx-toastr';
import {IAssetType} from '../../../_models/asset-type';
import {AssetService} from '../../../_services/asset.service';

@Component({
  selector: 'sm-rescuepack',
  templateUrl: './rescuepack.component.html',
  styleUrls: ['./rescuepack.component.css']
})
export class RescuepackComponent implements OnInit, OnChanges {
  equipmentState = 'rescue-pack unassigned';
  @Input() assignedRescuePacks: Assignment[];
  assetTypes: IAssetType[];

  constructor(private assetService: AssetService) { }

  ngOnInit() {
    this.assetService.getAssetTypesByClass('rescue-pack')
      .subscribe(response => {
        this.assetTypes = response;
      });
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedRescuePacks'] && this.assignedRescuePacks.length > 0) {
      this.equipmentState = 'rescue-pack assigned';
    }
    if (changes['assignedRescuePacks'] && this.assignedRescuePacks.length === 0) {
      this.equipmentState = 'rescue-pack unassigned';
    }
  }

}
