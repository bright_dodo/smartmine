import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RescuepackComponent } from './rescuepack.component';

describe('RescuepackComponent', () => {
  let component: RescuepackComponent;
  let fixture: ComponentFixture<RescuepackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RescuepackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RescuepackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
