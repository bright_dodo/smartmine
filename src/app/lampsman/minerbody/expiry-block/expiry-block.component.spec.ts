import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpiryBlockComponent } from './expiry-block.component';

describe('ExpiryBlockComponent', () => {
  let component: ExpiryBlockComponent;
  let fixture: ComponentFixture<ExpiryBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpiryBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpiryBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
