import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinerbodyComponent } from './minerbody.component';

describe('MinerbodyComponent', () => {
  let component: MinerbodyComponent;
  let fixture: ComponentFixture<MinerbodyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinerbodyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinerbodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
