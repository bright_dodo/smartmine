import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaplampComponent } from './caplamp.component';

describe('CaplampComponent', () => {
  let component: CaplampComponent;
  let fixture: ComponentFixture<CaplampComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaplampComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaplampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
