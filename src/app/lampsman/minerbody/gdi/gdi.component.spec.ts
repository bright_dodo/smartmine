import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GdiComponent } from './gdi.component';

describe('GdiComponent', () => {
  let component: GdiComponent;
  let fixture: ComponentFixture<GdiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GdiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GdiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
