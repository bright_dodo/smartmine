import {Component, Input, OnInit} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {AssetService} from '../../../_services/asset.service';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateAssetModalComponent} from '../../create/create-asset-modal/create-asset-modal.component';
import {IAssetType} from '../../../_models/asset-type';
import {
  ADD_RECENTLY_ALLOCATED_ASSET, ADD_RECENTLY_DEALLOCATED_ASSET, UPDATE_PERSON_ASSIGNMENTS,
} from '../../lampsman.action';
import {AllocateCreatedAssetModalComponent} from '../../allocate/allocate-created-asset-modal/allocate-created-asset-modal.component';
import {Asset} from '../../../_models/asset';
import {DeallocateConfirmationModalComponent} from '../../allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';
import {AvailableAssetsModalComponent} from '../../allocate/available-assets-modal/available-assets-modal.component';
import {AssetAssignmentHistoryModalComponent} from '../../asset/asset-assignment-history-modal/asset-assignment-history-modal.component';
import {Router} from '@angular/router';

@Component({
  selector: 'sm-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {
  operationalStatus = 'Operational';
  @Input() assignments: Assignment[];
  @Input() assetTypes: IAssetType[];
  person: Person;

  shiftOptions: string[] = ['Morning', 'Afternoon', 'Evening'];

  closeResult: string;

  constructor(private assetService: AssetService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private modalService: NgbModal,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
      // this.assetTypes = store.lampsman.assetClassTypes;
    });
  }

  ngOnInit() {
  }

  openCreateModal(assetType) {
    const modalRef = this.modalService.open(CreateAssetModalComponent);
    modalRef.componentInstance.assetType = assetType;
    modalRef.result.then((result) => {
      console.log('maresults anoti: ', result);
      if (result.success && this.person) {
        this.openAllocateModal(result.asset);
      }
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openAllocateModal(asset: Asset) {
    const allocateModalRef = this.modalService.open(AllocateCreatedAssetModalComponent);
    allocateModalRef.componentInstance.asset = asset;
    allocateModalRef.componentInstance.person = this.person;
  }

  openAvailableAssetsModal(assetType: IAssetType, assignmentType: string, isSpare: string) {
    const availableAssetsModalRef = this.modalService.open(AvailableAssetsModalComponent, {size: 'lg'});
    availableAssetsModalRef.componentInstance.assignmentType = assignmentType;
    availableAssetsModalRef.componentInstance.assetType = assetType.description;

    this.assetService.getAssetByTypeAndAllocationStatus(assetType.name, isSpare, 'false')
      .subscribe(response => {
        if (response._embedded) {
          availableAssetsModalRef.componentInstance.assets = response._embedded['assetResources'];
        }
      });

    availableAssetsModalRef.result.then(result => {
      if (result.success) {
        const ass = {assetId: result.assetId, personId: this.person.personId, status: 'allocated'};
        this.assetService.assignAsset(ass)
          .subscribe(response => {
            this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
            this.toastr.success('Asset was allocated successfully', 'Allocation Successful!');
            this.ngRedux.dispatch({type: ADD_RECENTLY_ALLOCATED_ASSET, asset: result});
          });
      }
    });
  }

  assetAssignmentHistoryReport(assetId: string) {
    this.router.navigate(['/reports/lms/allocation/history/asset', {asset: assetId}]);
  }

  openAssignmentHistoryModal(asset: Asset) {
    let assignHistory: Assignment[];
    this.assetService.getAssignmentHistoryForAsset(asset.assetId)
      .subscribe(response => {
        assignHistory = response._embedded['assignmentResources'];
      });
    const assignHistoryModaRef = this.modalService.open(AssetAssignmentHistoryModalComponent, {size: 'lg'});
    assignHistoryModaRef.componentInstance.asset = asset;
    assignHistoryModaRef.componentInstance.assignments = assignHistory;
  }

  deallocate(assignment: Assignment) {
    console.log(assignment);
    const deallocateModalRef = this.modalService.open(DeallocateConfirmationModalComponent);
    deallocateModalRef.componentInstance.assignment = assignment;
    deallocateModalRef.result.then(result => {
      if (result) {
        const ass = {id: assignment.assignmentId, personId: assignment.person.personId};
        this.assetService.deassignAsset(ass)
          .subscribe(response => {
            this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
            this.toastr.info('The asset has been deallocated', 'Asset Deassigned');
            this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: result});
          });
      }
    });

  }


}
