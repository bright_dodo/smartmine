import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LampsmanRoutingModule } from './lampsman-routing.module';
import {LampsmanDashboardComponent} from './lampsman-dashboard/lampsman-dashboard.component';
import { SearchComponent } from './search/search.component';
import { CreateComponent } from './create/create.component';
import { SearchPersonComponent } from './search/search-person/search-person.component';
import { SearchAssetComponent } from './search/search-asset/search-asset.component';
import { MinerbodyComponent } from './minerbody/minerbody.component';
import {SharedModule} from '../shared/shared.module';
import { CaplampComponent } from './minerbody/caplamp/caplamp.component';
import { DropdownComponent } from './minerbody/dropdown/dropdown.component';
import { GdiComponent } from './minerbody/gdi/gdi.component';
import { MiscComponent } from './minerbody/misc/misc.component';
import { RescuepackComponent } from './minerbody/rescuepack/rescuepack.component';
import { PdsComponent } from './minerbody/pds/pds.component';
import { PersonInformationComponent } from './person/person-information/person-information.component';
import { RecentAllocationsComponent } from './search/recent-allocations/recent-allocations.component';
import { PersonAllocationsComponent } from './person/person-allocations/person-allocations.component';
import { ExpiryBlockComponent } from './minerbody/expiry-block/expiry-block.component';
import { CreateChoiceComponent } from './create/create-choice/create-choice.component';
import { CreateAssetComponent } from './create/create-asset/create-asset.component';
import { CreatePersonComponent } from './create/create-person/create-person.component';
import { RecentlyCreatedPersonsComponent } from './create/recently-created-persons/recently-created-persons.component';
import { RecentlyCreatedAssetsComponent } from './create/recently-created-assets/recently-created-assets.component';
import { CreatePersonModalComponent } from './create/create-person-modal/create-person-modal.component';
import { CreateAssetModalComponent } from './create/create-asset-modal/create-asset-modal.component';
import { AllocateComponent } from './allocate/allocate.component';
import { AllocateCreatedAssetModalComponent } from './allocate/allocate-created-asset-modal/allocate-created-asset-modal.component';
import { DeallocateConfirmationModalComponent } from './allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';
import { AvailableAssetsModalComponent } from './allocate/available-assets-modal/available-assets-modal.component';
import { ChoiceComponent } from './create/choice/choice.component';
import { AssetDetailsModalComponent } from './asset/asset-details-modal/asset-details-modal.component';
import { ModifyAssetModalComponent } from './asset/modify-asset-modal/modify-asset-modal.component';
import { PersonDetailsModalComponent } from './person/person-details-modal/person-details-modal.component';
import { ModifyPersonModalComponent } from './person/modify-person-modal/modify-person-modal.component';
import { AssetAssignmentHistoryModalComponent } from './asset/asset-assignment-history-modal/asset-assignment-history-modal.component';
import { PersonSearchResultsComponent } from './search/search-person/person-search-results/person-search-results.component';

@NgModule({
  imports: [
    CommonModule,
    LampsmanRoutingModule,
    SharedModule
  ],
  declarations: [
    LampsmanDashboardComponent,
    SearchComponent,
    CreateComponent,
    SearchPersonComponent,
    SearchAssetComponent,
    MinerbodyComponent,
    CaplampComponent,
    DropdownComponent,
    GdiComponent,
    MiscComponent,
    RescuepackComponent,
    PdsComponent,
    PersonInformationComponent,
    RecentAllocationsComponent,
    PersonAllocationsComponent,
    ExpiryBlockComponent,
    CreateChoiceComponent,
    CreateAssetComponent,
    CreatePersonComponent,
    RecentlyCreatedPersonsComponent,
    RecentlyCreatedAssetsComponent,
    CreatePersonModalComponent,
    CreateAssetModalComponent,
    AllocateComponent,
    AllocateCreatedAssetModalComponent,
    DeallocateConfirmationModalComponent,
    AvailableAssetsModalComponent,
    ChoiceComponent,
    AssetDetailsModalComponent,
    ModifyAssetModalComponent,
    PersonDetailsModalComponent,
    ModifyPersonModalComponent,
    AssetAssignmentHistoryModalComponent,
    PersonSearchResultsComponent
  ],
  entryComponents: [
    CreatePersonModalComponent,
    CreateAssetModalComponent,
    AllocateCreatedAssetModalComponent,
    DeallocateConfirmationModalComponent,
    AvailableAssetsModalComponent,
    AssetDetailsModalComponent,
    ModifyAssetModalComponent,
    PersonDetailsModalComponent,
    ModifyPersonModalComponent,
    AssetAssignmentHistoryModalComponent
  ]
})
export class LampsmanModule { }
