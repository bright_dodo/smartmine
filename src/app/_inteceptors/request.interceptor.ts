import {Injectable, Injector} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../../app/_services/authentication.service';
import {IToken} from '../../app/_models/token';
import 'rxjs/add/operator/do';
import {Router} from '@angular/router';


@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor(private inj: Injector,
              private router: Router) {
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authService = this.inj.get(AuthenticationService);
    const token: IToken = authService.retrieveToken();

    if (token) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token)
      });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }
    // return next.handle(request);
    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          // redirect to the login route
          // this.router.navigate(['/login']);
          // or store request and try to refresh token
          authService.collectFailedRequest(request);
          authService.refreshAccessToken()
            .subscribe(
              (authToken: any) => {
                authService.setSession(authToken);
                next.handle(request);
              },
              (error: any) => {
                this.router.navigate(['/login']);
              });
        }
      }
    });
  }
}
