import { Component, OnInit } from '@angular/core';
import {AssetService} from '../../../_services/asset.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AssetSummaryModalComponent} from '../asset-summary-modal/asset-summary-modal.component';
import {AssetTypeSummaryModalComponent} from '../asset-type-summary-modal/asset-type-summary-modal.component';

@Component({
  selector: 'sm-asset-type-board',
  templateUrl: './asset-type-board.component.html',
  styleUrls: ['./asset-type-board.component.css']
})
export class AssetTypeBoardComponent implements OnInit {
  assetType = false;
  assetTypeStatistics: any[];
  assetTypeTotals: number;

  constructor(private assetService: AssetService,
              private modalService: NgbModal) { }

  ngOnInit() {
    this.assetService.getAssetTypeCount()
      .subscribe(response => {
          this.assetTypeStatistics = response;
          this.assetTypeTotals = this.calculateAssetTypeTotals(response);
        });
  }

  calculateAssetTypeTotals(data) {
    let total = 0;
    data.forEach(d => {
      total += d.totalCount;
    });
    return total;
  }

  openAssetSummaryModal() {
    const assetSummaryModalRef = this.modalService.open(AssetSummaryModalComponent, {size: 'lg'});
  }

  openAssetTypeSummaryModal() {
    const assetSummaryModalRef = this.modalService.open(AssetTypeSummaryModalComponent);
  }
}
