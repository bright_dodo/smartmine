import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetTypeBoardComponent } from './asset-type-board.component';

describe('AssetTypeBoardComponent', () => {
  let component: AssetTypeBoardComponent;
  let fixture: ComponentFixture<AssetTypeBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetTypeBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTypeBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
