import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-asset-type-summary-modal',
  templateUrl: './asset-type-summary-modal.component.html',
  styleUrls: ['./asset-type-summary-modal.component.css']
})
export class AssetTypeSummaryModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
