import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetTypeSummaryModalComponent } from './asset-type-summary-modal.component';

describe('AssetTypeSummaryModalComponent', () => {
  let component: AssetTypeSummaryModalComponent;
  let fixture: ComponentFixture<AssetTypeSummaryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetTypeSummaryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetTypeSummaryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
