import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentsBoardComponent } from './assignments-board.component';

describe('AssignmentsBoardComponent', () => {
  let component: AssignmentsBoardComponent;
  let fixture: ComponentFixture<AssignmentsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
