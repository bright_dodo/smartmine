import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementsBoardComponent } from './movements-board.component';

describe('MovementsBoardComponent', () => {
  let component: MovementsBoardComponent;
  let fixture: ComponentFixture<MovementsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
