import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-movements-board',
  templateUrl: './movements-board.component.html',
  styleUrls: ['./movements-board.component.css']
})
export class MovementsBoardComponent implements OnInit {
  movement = false;
  movements: any[];
  constructor() { }

  ngOnInit() {
  }

}
