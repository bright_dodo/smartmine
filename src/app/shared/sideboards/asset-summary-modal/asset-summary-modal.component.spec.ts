import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSummaryModalComponent } from './asset-summary-modal.component';

describe('AssetSummaryModalComponent', () => {
  let component: AssetSummaryModalComponent;
  let fixture: ComponentFixture<AssetSummaryModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSummaryModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetSummaryModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
