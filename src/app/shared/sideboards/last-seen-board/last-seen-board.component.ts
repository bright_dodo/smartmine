import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-last-seen-board',
  templateUrl: './last-seen-board.component.html',
  styleUrls: ['./last-seen-board.component.css']
})
export class LastSeenBoardComponent implements OnInit {
  assetsLastSeen = true;
  lastSeenStatistics = [{name: 'Surface', total: 5213}, {name: 'Underground', total: 954}];
  constructor() { }

  ngOnInit() {
  }

}
