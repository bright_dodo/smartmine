import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementsBoardComponent } from './measurements-board.component';

describe('MeasurementsBoardComponent', () => {
  let component: MeasurementsBoardComponent;
  let fixture: ComponentFixture<MeasurementsBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementsBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementsBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
