import { Component, OnInit } from '@angular/core';
import {APP_NAME} from '../../../../config/app.config';

@Component({
  selector: 'sm-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {
  appTitle = APP_NAME;
  constructor() { }

  ngOnInit() {
  }

}
