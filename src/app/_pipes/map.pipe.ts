import { Pipe, PipeTransform } from '@angular/core';
import {IAttributeMapEntry} from '../_models/attribute-map-entry';
import {IAttr} from '../_models/IAttr';

@Pipe({
  name: 'map'
})
export class MapPipe implements PipeTransform {

  transform(inputs: IAttr[], args?: any): any {
    let val: any;
    if (inputs !== undefined) {
      Object.entries(inputs).forEach(([key, value]) => {
        if (value.name === args) {
          val = value.value;
        }
      });
    }
    return val;
  }

}
