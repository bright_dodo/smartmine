import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'attributeTransform'
})
export class AttributeTransformPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return Object.keys(value).map(function (key: any): any {
      const pair: any = {};
      const k: any = 'key';
      const v: any = 'value';

      pair[k] = key;
      if (value[key] != null) {
        pair[v] = value[key];
      } else {
        pair[v] = '';
      }
      return pair;
    });
  }

}
