import { Component } from '@angular/core';
import {Spinkit} from 'ng-http-loader';

@Component({
  selector: 'sm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public spinkit = Spinkit;
  title = 'app';
}
